﻿using ItemsSystem;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.ItemComponent
{
    public class Item : EntityComponent
    {
        #region Variables

        private AbstractItem abstractItem;
        public AbstractItem AbstractItem
        {
            get { return abstractItem; }
            set
            {
                abstractItem = value;
                if (abstractItem != null)
                {
                    Entity.SpriteRenderer.sprite = abstractItem.ItemSprite;
                    Entity.Name = abstractItem.ItemName;
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogError("Setting null AbstractItem in ItemComponent");
#endif
                    Entity.SpriteRenderer.sprite =null;
                    Entity.Name = "EmptyItem";
                }
            }
        }

        #endregion

    }
}
