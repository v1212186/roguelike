﻿using ActionsSystem;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.ActorComponent
{
    public class PlayerActor : Actor
    {

        #region Variables

        protected AbstractAction abstractAction;
        //TODO переделать в метод, который тру или фалс если удалось сменить действие, плюс отслеживает кансел при одинаковом действии
        public bool SetAbstractActionNew(AbstractAction _action)
        {
            if (abstractAction == null || !abstractAction.RecentActionResult.IsPerforming)
            {
                abstractAction = _action;
                return true;
            }
#if UNITY_EDITOR
            else
            {
                Debug.LogError("Can't cancel action at current state");
            }
#endif
            return false;
        }

        //TODO заюзать
        //private InputHandler inputHandler;

        #endregion

        #region Monobehaviour methods

        private void Start()
        {
            //inputHandler = new InputHandler();
        }

        #endregion

        #region Methods

        public override void ProcessActor()
        {
            if (Input.GetKeyDown(KeyCode.Z))
            {
                abstractAction = new EmptyAction(Entity, true);
            }
            if (Input.GetKeyDown(KeyCode.X))
            {
                ForcePerformAction(new MoveAction(Entity, Vector2Int.left));
            }

            Debug.Log("Processing as " + Entity);
            ActionResult actionResult = abstractAction.ProcessAction();
            Debug.Log(actionResult.Flags);

            //TODO передавать ли инпут в экшон?
            if (actionResult.IsPerforming || actionResult.IsAwaitingInput)
            {
                return;
            }

            if (actionResult.IsDone)
            {
                if (abstractAction.EndsTurn)
                {
                    EndTurn();
                }
                else
                {
                    ResetAction();
                }
                return;
            }

            if (actionResult.IsFailed)
            {
                if (actionResult.AlternateAction != null)
                {
                    abstractAction = actionResult.AlternateAction;
                }
                else
                {
                    ResetAction();
                }
                return;
            }

        }

        private void ResetAction()
        {
            SetAbstractActionNew(new MoveAction(Entity));
        }

        public override void StartTurn()
        {
            base.StartTurn();
            ResetAction();
        }

        #endregion

    }
}
