﻿using System;
using System.Collections;
using ActionsSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem.EntityComponents.ActorComponent
{

    public class ActorTurnChange : EventArgs
    {

        public enum ActorTurnChangeType
        {
            Started, Ended
        }

        #region Variables

        public readonly ActorTurnChangeType actorTurnChangeType;

        #endregion

        #region Constructor

        public ActorTurnChange(ActorTurnChangeType _actorTurnChangeType)
        {
            actorTurnChangeType = _actorTurnChangeType;
        }

        #endregion

    }

    [DisallowMultipleComponent]
    public abstract class Actor : EntityComponent
    {

        #region Variables

        #region Events
        //TODO как-нибудь проверить, отписываются ли вообще от этих эвентов
        public readonly GameEvent<Entity, ActorTurnChange> OnActorTurnChanged = new GameEvent<Entity, ActorTurnChange>();

        #endregion

        #endregion

        #region Methods

        public abstract void ProcessActor();

        public void ForcePerformAction(AbstractAction _abstractAction)
        {
            StartCoroutine(ForcePerformActionCoroutine(_abstractAction));
        }

        private IEnumerator ForcePerformActionCoroutine(AbstractAction _abstractAction)
        {
            bool actionInProcess = true;
            AbstractAction abstractAction = _abstractAction;
            WaitForEndOfFrame wfeof = new WaitForEndOfFrame();

            while (actionInProcess)
            {
                ActionResult actionResult = abstractAction.ProcessAction();
                if (actionResult.IsAwaitingInput)
                {
#if UNITY_EDITOR
                    Debug.LogError("Waiting for input in forced action");
#endif
                    yield break;
                }
                if (actionResult.IsPerforming)
                {
                    yield return wfeof;
                }
                if (actionResult.IsDone)
                {
                    actionInProcess = false;
                }
                if (actionResult.IsFailed)
                {
                    if (actionResult.AlternateAction != null)
                    {
                        abstractAction = actionResult.AlternateAction;
                    }
                    else
                    {
                        actionInProcess = false;
                    }
                }
            }
        }

        public virtual void StartTurn()
        {
            OnActorTurnChanged.Raise(this.Entity, new ActorTurnChange(ActorTurnChange.ActorTurnChangeType.Started));
        }

        public virtual void EndTurn()
        {
            SpendEnergy();
            OnActorTurnChanged.Raise(this.Entity, new ActorTurnChange(ActorTurnChange.ActorTurnChangeType.Ended));
        }

        //TODO через метод в чарактер атрибутах
        protected void SpendEnergy()
        {
            Entity.CharacterAttributes.EnergyAttribute.ChangeCurrentValue(-Entity.CharacterAttributes.EnergyAttribute
                .FinalValue);
        }

        public void GainEnergy(float _value = +1.0f)
        {
            Entity.CharacterAttributes.EnergyAttribute.ChangeCurrentValue(_value);
        }

        #endregion
    }
}
