﻿using System.Collections;
using ActionsSystem;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.ActorComponent
{
    //TODo зачем вообще отдельныйкласс писал, не помню
    public class SimpleActor : EntityComponent {

        #region Methods

        public void ForcePerformAction(AbstractAction _abstractAction)
        {
            StartCoroutine(ForcePerformActionCoroutine(_abstractAction));
        }

        private IEnumerator ForcePerformActionCoroutine(AbstractAction _abstractAction)
        {
            bool actionInProcess = true;
            AbstractAction abstractAction = _abstractAction;
            WaitForEndOfFrame wfeof = new WaitForEndOfFrame();

            while (actionInProcess)
            {
                ActionResult actionResult = abstractAction.ProcessAction();
                if (actionResult.IsAwaitingInput)
                {
#if UNITY_EDITOR
                    Debug.LogError("Waiting for input in forced action");
#endif
                    yield break;
                }
                if (actionResult.IsPerforming)
                {
                    yield return wfeof;
                }
                if (actionResult.IsDone)
                {
                    actionInProcess = false;
                }
                if (actionResult.IsFailed)
                {
                    if (actionResult.AlternateAction != null)
                    {
                        abstractAction = actionResult.AlternateAction;
                    }
                    else
                    {
                        actionInProcess = false;
                    }
                }
            }
        } 

        #endregion
    }
}
