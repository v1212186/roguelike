﻿using System;
using System.Collections.Generic;
using EntitiesSystem.EntityComponents.ActorComponent;
using LevelsSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem.EntityComponents.TriggerComponent
{
    //TODo проверить когда энтити движущийся
    //TODO баг, когда энтити уничтожается то остается в листе, нужен эвент при уничтожении энтити, лучше даже энтити менеджер
    //TODO добавить визуализацию области
    [DisallowMultipleComponent]
    public abstract class Trigger : EntityComponent
    {
        #region Variables

        [SerializeField]
        private Vector2Int[] triggerArea;

        [SerializeField]
        private bool ignoreSelf = true;

        [SerializeField]
        private List<Entity> entitiesInTriggerArea = new List<Entity>();

        #endregion

        #region Methods

        public void Initialize(Vector2Int[] _triggerArea, bool _ignoreSelf)
        {
            triggerArea = _triggerArea;
            ignoreSelf = _ignoreSelf;
            Entity.OnEntityPlacedOnLevel.Add(OnEntityPlacedOnLevel);
            Entity.OnEntityPositionChanged.Add(OnEntityPositionChanged);
        }

        private void OnEntityPositionChanged(Entity _entity, ValueChangeEventArgs<Vector2Int> _valueChangeEventArgs)
        {
            ForceCheckEntities();
        }

        protected void ForceCheckEntities()
        {
            foreach (Entity entity in Entity.Level.EntitiesReadOnly)
            {
                if (CheckIfPositionInArea(entity.Position))
                {
                    OnTriggerAreaEnter(entity);
                }
                else
                {
                    OnTriggerAreaExit(entity);
                }
            }
        }

        private void OnEntityPlacedOnLevel(Entity _entity, LevelChange _levelChange)
        {
            if (_levelChange.PreviousLevel != null)
            {
                _levelChange.PreviousLevel.OnEntityPositionChangedGlobal.Remove(OnEntityPositionChangedGlobal);
            }
            _levelChange.CurrentLevel.OnEntityPositionChangedGlobal.Add(OnEntityPositionChangedGlobal);
            _levelChange.CurrentLevel.OnEntityRegistrationGlobal.Add(OnEntityRegistrationGlobal);

            ForceCheckEntities();
        }

        private void OnEntityRegistrationGlobal(Entity _entity, EntityRegistration _entityRegistration)
        {
            if (_entityRegistration.RegistationType == RegistationType.Unregister)
            {
                OnTriggerAreaExit(_entity);
            }
        }

        private void OnEntityPositionChangedGlobal(Entity _entity, ValueChangeEventArgs<Vector2Int> _valueChangeEventArgs)
        {
            bool wasInTriggerArea = CheckIfPositionInArea(_valueChangeEventArgs.OldValue);
            bool nowInTriggerArea = CheckIfPositionInArea(_valueChangeEventArgs.NewValue);
            if (wasInTriggerArea && nowInTriggerArea)
            {
                OnMovedInTriggerArea(_entity);
            }
            if (!wasInTriggerArea && nowInTriggerArea)
            {
                OnTriggerAreaEnter(_entity);
            }
            if (wasInTriggerArea && !nowInTriggerArea)
            {
                OnTriggerAreaExit(_entity);
            }
        }

        protected bool CheckIfPositionInArea(Vector2Int _position)
        {
            for (int i = 0; i < triggerArea.Length; i++)
            {
                if (Entity.Position + triggerArea[i] == _position)
                {
                    return true;
                }
            }
            return false;
        }

        protected void OnTriggerAreaEnter(Entity _entity)
        {
            if (ignoreSelf && _entity.Equals(Entity) || entitiesInTriggerArea.Contains(_entity))
            {
                return;
            }
            entitiesInTriggerArea.Add(_entity);
            if (_entity.Actor)
            {
                _entity.Actor.OnActorTurnChanged.Add(OnActorTurnChanged);
            }
            OnEntityEnteredTriggerArea(_entity);
        }

        private void OnActorTurnChanged(Entity _entity, ActorTurnChange _actorTurnChange)
        {
            if (_actorTurnChange.actorTurnChangeType == ActorTurnChange.ActorTurnChangeType.Started)
            {
                OnNewTurnInTriggerArea(_entity);
            }
        }

        protected void OnTriggerAreaExit(Entity _entity)
        {
            if (!entitiesInTriggerArea.Remove(_entity))
            {
                return;
            }
            if (_entity.Actor)
            {
                _entity.Actor.OnActorTurnChanged.Remove(OnActorTurnChanged);
            }
            OnEntityExitedTriggerArea(_entity);
        }

        protected void OnMovedInTriggerArea(Entity _entity)
        {
            OnEntityMovedInTriggerArea(_entity);
        }

        protected void OnNewTurnInTriggerArea(Entity _entity)
        {
            OnEntityNewTurnInTriggerArea(_entity);
        }

        protected abstract void OnEntityEnteredTriggerArea(Entity _entity);

        protected abstract void OnEntityExitedTriggerArea(Entity _entity);

        protected abstract void OnEntityMovedInTriggerArea(Entity _entity);

        protected abstract void OnEntityNewTurnInTriggerArea(Entity _entity);

        #endregion


    }
}
