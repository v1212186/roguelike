﻿using UnityEngine;

namespace EntitiesSystem.EntityComponents.TriggerComponent
{
    public class Trap : Trigger {

        #region Methods
        protected override void OnEntityEnteredTriggerArea(Entity _entity)
        {
            if (_entity.CharacterAttributes)
            {
                _entity.CharacterAttributes.HealthAttribute.ChangeCurrentValue(-5);
            }
        }

        protected override void OnEntityExitedTriggerArea(Entity _entity)
        {
        }

        protected override void OnEntityMovedInTriggerArea(Entity _entity)
        {
        }

        protected override void OnEntityNewTurnInTriggerArea(Entity _entity)
        {
        } 

        #endregion
    }
}
