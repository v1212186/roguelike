﻿using StatusEffectsSystem;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.TriggerComponent
{
    public class Aura : Trigger {

        #region Variables

        [SerializeField]
        private AbstractStatusEffect statusEffect;

        #endregion

        #region Methods

        public void SetStatusEffect(AbstractStatusEffect _abstractStatusEffect)
        {
            statusEffect = _abstractStatusEffect;
        }

        protected override void OnEntityEnteredTriggerArea(Entity _entity)
        {
            if (_entity.StatusEffectsSet)
            {
                _entity.StatusEffectsSet.ApplyStatusEffect(statusEffect, Entity);
            }
        }

        protected override void OnEntityExitedTriggerArea(Entity _entity)
        {
            if (_entity.StatusEffectsSet)
            {
                _entity.StatusEffectsSet.RemoveAllStatusEffectsFromSource(Entity);
            }
        }

        protected override void OnEntityMovedInTriggerArea(Entity _entity)
        {
        }

        protected override void OnEntityNewTurnInTriggerArea(Entity _entity)
        {
            if (_entity.StatusEffectsSet)
            {
                _entity.StatusEffectsSet.ApplyStatusEffect(statusEffect, Entity);
            }
        } 
        #endregion
    }
}
