﻿using System;
using System.Collections.Generic;
using AttributesSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem.EntityComponents.CharacterAttributesComponent
{
    //TODO может заюзать потом для оптимизации интерфейса
    //public class CharacterAttributeChange : EventArgs
    //{
    //    #region Variables

    //    public readonly AbstractAttribute abstractAttribute;

    //    #endregion

    //    #region Constructor

    //    public CharacterAttributeChange(AbstractAttribute _abstractAttribute)
    //    {
    //        abstractAttribute = _abstractAttribute;
    //    }

    //    #endregion
    //}

    [DisallowMultipleComponent]
    public class CharacterAttributes : EntityComponent
    {
        #region Variables
        [SerializeField]
        private HealthAttribute healthAttribute;
        public HealthAttribute HealthAttribute { get { return healthAttribute; } }

        [SerializeField]
        private EnergyAttribute energyAttribute;
        public EnergyAttribute EnergyAttribute { get { return energyAttribute; } }

        [SerializeField]
        private AttackAttribute attackAttribute;
        public AttackAttribute AttackAttribute { get { return attackAttribute; } }

        [SerializeField]
        private DefenceAttribute defenceAttribute;
        public DefenceAttribute DefenceAttribute { get { return defenceAttribute; } }

        [SerializeField]
        private CriticalHitChanceAttribute criticalHitChanceAttribute;
        public CriticalHitChanceAttribute CriticalHitChanceAttribute { get { return criticalHitChanceAttribute; } }

        [SerializeField]
        private HitChanceAttribute hitChanceAttribute;
        public HitChanceAttribute HitChanceAttribute { get { return hitChanceAttribute; } }

        public GameEvent<Entity, EventArgs> OnCharacterAttributesChanged = new GameEvent<Entity, EventArgs>();

        #endregion

        #region Methods

        public void Initialize(HealthAttribute _healthAttribute, EnergyAttribute _energyAttribute, AttackAttribute _attackAttribute,
            DefenceAttribute _defenceAttribute, CriticalHitChanceAttribute _criticalHitChanceAttribute, HitChanceAttribute _hitChanceAttribute)
        {
            healthAttribute = _healthAttribute;
            energyAttribute = _energyAttribute;
            attackAttribute = _attackAttribute;
            defenceAttribute = _defenceAttribute;
            criticalHitChanceAttribute = _criticalHitChanceAttribute;
            hitChanceAttribute = _hitChanceAttribute;
        }

        public void AddAttributeModifier(AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Health:
                    healthAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.Energy:
                    energyAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.Attack:
                    attackAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.Defence:
                    defenceAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.CriticalHit:
                    criticalHitChanceAttribute.AddModifier(_attributeModifier);
                    break;
                case AttributeType.HitChance:
                    hitChanceAttribute.AddModifier(_attributeModifier);
                    break;
            }
            OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
        }

        public bool RemoveAllModifiersFromSource(object _source)
        {
            bool isRemoved = false;
            List<AbstractAttribute> attributes = new List<AbstractAttribute>
            { healthAttribute, energyAttribute, attackAttribute, defenceAttribute, criticalHitChanceAttribute, hitChanceAttribute };
            for (int i = 0; i < attributes.Count; i++)
            {
                if (attributes[i].RemoveAllAttributeModifiersFromSource(_source))
                {
                    isRemoved = true;
                }
            }
            if (isRemoved)
            {
                OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
            }
            return isRemoved;
        }

        //TODO причесать код
        public bool RemoveAttributeModifier(AttributeModifier _attributeModifier)
        {
            switch (_attributeModifier.AttributeType)
            {
                case AttributeType.Health:
                    OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
                    return healthAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.Energy:
                    OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
                    return energyAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.Attack:
                    OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
                    return attackAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.Defence:
                    OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
                    return defenceAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.CriticalHit:
                    OnCharacterAttributesChanged.Raise(this.Entity, EventArgs.Empty);
                    return criticalHitChanceAttribute.RemoveModifier(_attributeModifier);
                case AttributeType.HitChance:
                    return hitChanceAttribute.RemoveModifier(_attributeModifier);
                default: return false;
            }
        }

        #endregion
    }
}
