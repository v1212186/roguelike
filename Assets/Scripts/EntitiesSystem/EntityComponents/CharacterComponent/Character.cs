﻿using UnityEngine;

namespace EntitiesSystem.EntityComponents.CharacterComponent
{
    [DisallowMultipleComponent]
    public class Character : EntityComponent {

        #region Variables

        public string Name
        {
            get { return Entity.Name; }
            set { Entity.Name = value; }
        }

        #endregion

    }
}
