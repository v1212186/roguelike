﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ItemsSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem.EntityComponents.InventoryComponent
{
    //TODO решить что делать если нельзя снять итем так как нет места в инвентаре
    [DisallowMultipleComponent]
    public class Inventory : EntityComponent
    {
        #region Variables
        private EquippableItem helmetItem;
        public EquippableItem HelmetItem { get { return helmetItem; } }
        private EquippableItem armorItem;
        public EquippableItem ArmorItem { get { return armorItem; } }
        private EquippableItem weaponItem;
        public EquippableItem WeaponItem { get { return weaponItem; } }
        private EquippableItem glovesItem;
        public EquippableItem GlovesItem { get { return glovesItem; } }
        private EquippableItem bootsItem;
        public EquippableItem BootsItem { get { return bootsItem; } }

        private const int InventoryCapacity = 54;
        private readonly List<AbstractItem> items = new List<AbstractItem>();
        private ReadOnlyCollection<AbstractItem> itemsReadOnly;
        public ReadOnlyCollection<AbstractItem> Items
        {
            get
            {
                if (itemsReadOnly == null)
                {
                    itemsReadOnly = items.AsReadOnly();
                }
                return itemsReadOnly;
            }
        }
        #region Events

        public readonly GameEvent<Entity, EventArgs> OnInventoryChanged = new GameEvent<Entity, EventArgs>();

        #endregion

        #endregion

        #region Methods

        public bool IsItemEquipped(EquippableItem _equippableItem)
        {
            return (_equippableItem == helmetItem || _equippableItem == armorItem || _equippableItem == weaponItem ||
                   _equippableItem == glovesItem || _equippableItem == bootsItem) && _equippableItem != null;
        }

        public bool EquipItem(EquippableItem _itemToEquip, out EquippableItem _previouslyEquippedItem)
        {
            _previouslyEquippedItem = null;
            if (_itemToEquip == null)
            {
                return false;
            }
            if (RemoveItem(_itemToEquip))
            {
                switch (_itemToEquip.equipmentSlot)
                {
                    case EquipmentSlot.Helmet:
                        if (UnequipItem(helmetItem))
                        {
                            _previouslyEquippedItem = helmetItem;
                        }
                        helmetItem = _itemToEquip;
                        break;
                    case EquipmentSlot.Armor:
                        if (UnequipItem(armorItem))
                        {
                            _previouslyEquippedItem = armorItem;
                        }
                        armorItem = _itemToEquip;
                        break;
                    case EquipmentSlot.Weapon:
                        if (UnequipItem(weaponItem))
                        {
                            _previouslyEquippedItem = weaponItem;
                        }
                        weaponItem = _itemToEquip;
                        break;
                    case EquipmentSlot.Gloves:
                        if (UnequipItem(glovesItem))
                        {
                            _previouslyEquippedItem = glovesItem;
                        }
                        glovesItem = _itemToEquip;
                        break;
                    case EquipmentSlot.Boots:
                        if (UnequipItem(bootsItem))
                        {
                            _previouslyEquippedItem = bootsItem;
                        }
                        bootsItem = _itemToEquip;
                        break;
                    default:
                        break;
                }
                if (Entity.CharacterAttributes != null)
                {
                    for (int i = 0; i < _itemToEquip.AttributeModifiers.Count; i++)
                    {
                        Entity.CharacterAttributes.AddAttributeModifier(_itemToEquip.AttributeModifiers[i]);
                    }
                }
#if UNITY_EDITOR
                else
                {
                    Debug.LogError("Can't add or remove attribute modifiers from equippable items. No [CharacterAttributes] entity component at: " + this);
                }
#endif
                OnInventoryChanged.Raise(this.Entity, EventArgs.Empty);
            }
            return true;
        }

        public bool UnequipItem(EquippableItem _equippableItem)
        {
            if (IsFull() || _equippableItem == null)
            {
                return false;
            }

            if (AddItem(_equippableItem))
            {
                switch (_equippableItem.equipmentSlot)
                {
                    case EquipmentSlot.Helmet:
                        helmetItem = null;
                        break;
                    case EquipmentSlot.Armor:
                        armorItem = null;
                        break;
                    case EquipmentSlot.Weapon:
                        weaponItem = null;
                        break;
                    case EquipmentSlot.Gloves:
                        glovesItem = null;
                        break;
                    case EquipmentSlot.Boots:
                        bootsItem = null;
                        break;
                    default:
                        break;
                }
                if (Entity.CharacterAttributes != null)
                {
                    Entity.CharacterAttributes.RemoveAllModifiersFromSource(_equippableItem);
                }
#if UNITY_EDITOR
                else
                {
                    Debug.LogError("Can't add or remove attribute modifiers from equippable items. No [CharacterAttributes] entity component at: " + this);
                }
#endif
                OnInventoryChanged.Raise(this.Entity, EventArgs.Empty);
                return true;
            }

            return false;
        }

        public bool AddItem(AbstractItem _item)
        {
            if (IsFull())
            {
                return false;
            }

            items.Add(_item);
            OnInventoryChanged.Raise(this.Entity, EventArgs.Empty);
            return true;
        }

        public bool RemoveItem(AbstractItem _item)
        {
            if (IsItemEquipped(_item as EquippableItem))
            {
                if (!UnequipItem(_item as EquippableItem))
                {
                    return false;
                }
            }
            if (items.Remove(_item))
            {
                OnInventoryChanged.Raise(this.Entity, EventArgs.Empty);
                return true;
            }
            return false;
        }

        public bool IsFull()
        {
            return items.Count >= InventoryCapacity;
        }

        #endregion

    }
}
