﻿using System;
using ActionsSystem;
using EntitiesSystem.EntityComponents.ActorComponent;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.InteractableComponent
{
    //TODO сделать чтобы дверь сама закрывалась через несколько ходов
    public class DoorInteractable : Interactable
    {

        #region Variables

        private Sprite[] doorOpenedClosedSprites;

        private bool isClosed;
        public bool IsClosed
        {
            get { return isClosed; }
            set
            {
                isClosed = value;
                if (isClosed)
                {
                    Entity.SpriteRenderer.sprite = doorOpenedClosedSprites[0];
                }
                else
                {
                    Entity.SpriteRenderer.sprite = doorOpenedClosedSprites[1];
                }
            }
        }

        #endregion

        #region Methods

        public void Initialize(Sprite[] _doorOpenedClosedSprites, bool _isClosed)
        {
            doorOpenedClosedSprites = _doorOpenedClosedSprites;
            ChangeDoorState(_isClosed);
        }

        protected void ChangeDoorState(bool _isClosed)
        {
            IsClosed = _isClosed;
            Entity.IsBlockingView = IsClosed;
            Entity.IsCollidable = IsClosed;
        }

        public override bool Interact(Entity _actor)
        {
            ChangeDoorState(!IsClosed);
            return false;
        }

        #endregion
    }
}
