﻿using UnityEngine;

namespace EntitiesSystem.EntityComponents.InteractableComponent
{
    [DisallowMultipleComponent]
    public abstract class Interactable : EntityComponent {

        #region Methods

        public abstract bool Interact(Entity _actor);

        #endregion

    }
}
