﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EntitiesSystem.EntityComponents.ActorComponent;
using StatusEffectsSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem.EntityComponents.StatusEffectsComponent
{
    public class StatusEffectChange : EventArgs
    {
        public enum StatusEffectChangeType
        {
            Applied, Removed
        }

        #region Varibles

        public readonly AbstractStatusEffect abstractStatusEffect;

        public readonly StatusEffectChangeType statusEffectChangeType;

        #endregion

        #region Constructor

        public StatusEffectChange(AbstractStatusEffect _abstractStatusEffect,
            StatusEffectChangeType _statusEffectChangeType)
        {
            abstractStatusEffect = _abstractStatusEffect;
            statusEffectChangeType = _statusEffectChangeType;
        }

        #endregion

    }



    [DisallowMultipleComponent]
    public class StatusEffectsSet : EntityComponent
    {

        #region Variables

        private readonly List<AbstractStatusEffect> statusEffects = new List<AbstractStatusEffect>();
        private ReadOnlyCollection<AbstractStatusEffect> statusEffectsReadOnly;
        public ReadOnlyCollection<AbstractStatusEffect> StatusEffects
        {
            get
            {
                if (statusEffectsReadOnly == null)
                {
                    statusEffectsReadOnly = statusEffects.AsReadOnly();
                }
                return statusEffectsReadOnly;
            }
        }

        public GameEvent<Entity, StatusEffectChange> OnStatusEffectsChanged = new GameEvent<Entity, StatusEffectChange>();
        #endregion

        #region Methods

        public void SubscribeToEvents()
        {
            Entity.Actor.OnActorTurnChanged.Add(OnActorTurnChanged);
        }

        private void OnActorTurnChanged(Entity _entity, ActorTurnChange _actorTurnChange)
        {
            if (_actorTurnChange.actorTurnChangeType == ActorTurnChange.ActorTurnChangeType.Started)
            {
                OnActorTurnStarted();
            }
        }

        public void OnActorTurnStarted()
        {
            ChangeStatusEffectsDuration(-1);
        }

        public void ChangeStatusEffectsDuration(int _value)
        {
            for (int i = statusEffects.Count - 1; i >= 0; i--)
            {
                statusEffects[i].ChangeDuration(_value);
            }
            CheckStatusEffectsForExpiration();
        }

        private void CheckStatusEffectsForExpiration()
        {
            for (int i = statusEffects.Count - 1; i >= 0; i--)
            {
                if (statusEffects[i].CurrentDuration <= 0)
                {
                    RemoveStatusEffect(statusEffects[i]);
                }
            }
        }

        //TODO обновление дюрейшена при применении того же эффекта
        public bool ApplyStatusEffect(AbstractStatusEffect _statusEffect, object _source)
        {
            if (statusEffects.Contains(_statusEffect))
            {
                return false;
            }
            bool isApplied = false;
            _statusEffect.SetSource(_source);
            if (_statusEffect.ApplyStatusEffect(Entity))
            {
                statusEffects.Add(_statusEffect);
                isApplied = true;
                OnStatusEffectsChanged.Raise(this.Entity, new StatusEffectChange(_statusEffect, StatusEffectChange.StatusEffectChangeType.Applied));
            }
            CheckStatusEffectsForExpiration();
            return isApplied;
        }

        public void RemoveStatusEffect(AbstractStatusEffect _statusEffect)
        {
            if (!statusEffects.Remove(_statusEffect))
            {
                return;
            }
            if (_statusEffect.RemoveStatusEffect(Entity))
            {
                OnStatusEffectsChanged.Raise(this.Entity, new StatusEffectChange(_statusEffect, StatusEffectChange.StatusEffectChangeType.Removed));
            }
            CheckStatusEffectsForExpiration();
        }

        public void RemoveAllStatusEffectsFromSource(object _source)
        {
            for (int i = statusEffects.Count - 1; i >= 0; i--)
            {
                if (statusEffects[i].Source == _source)
                {
                    RemoveStatusEffect(statusEffects[i]);
                }
            }
        }

        #endregion


    }
}
