﻿using UnityEngine;

namespace EntitiesSystem.EntityComponents
{
    //TODO добавить флаг инициализации и если обращаются к неинициализированному компоненту, то инициализировать дефолтными значениями
    public abstract class EntityComponent : MonoBehaviour
    {
        #region Variables
        private Entity entity;
        public Entity Entity
        {
            get
            {
                if (entity == null)
                {
                    entity = GetComponent<Entity>();
                }
                return entity;
            }
        }
        #endregion

        #region Methods
        protected virtual void Awake()
        {
            entity = GetComponent<Entity>();
            if (entity == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Adding Entity Component to GameObject without Entity: " + this);
#endif
                Destroy(this);
            }
        } 
        #endregion
    }
}
