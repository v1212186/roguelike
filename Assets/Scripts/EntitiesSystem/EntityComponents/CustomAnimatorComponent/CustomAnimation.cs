﻿using System;
using UnityEngine;

namespace EntitiesSystem.EntityComponents.CustomAnimatorComponent
{
    public enum AnimationName
    {
        Idle, Attack, Hit, Walk
    }

    [Serializable]
    public class CustomAnimation {
        #region Variables
        [SerializeField]
        private AnimationName name;
        public AnimationName Name { get { return name; } }
        [SerializeField]
        private float deltaTime;
        public float DeltaTime { get { return deltaTime; } }
        [SerializeField]
        private Sprite[] frames;
        public Sprite[] Frames { get { return frames; } }
        #endregion

        #region Methods
        public CustomAnimation(AnimationName _name, float _deltaTime, Sprite[] _frames)
        {
            name = _name;
            deltaTime = _deltaTime;
            frames = _frames;
        } 
        #endregion
    }
}
