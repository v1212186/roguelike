﻿using System;
using EntitiesSystem.EntityComponents.ActorComponent;
using EntitiesSystem.EntityComponents.CharacterAttributesComponent;
using EntitiesSystem.EntityComponents.CharacterComponent;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using EntitiesSystem.EntityComponents.InteractableComponent;
using EntitiesSystem.EntityComponents.InventoryComponent;
using EntitiesSystem.EntityComponents.ItemComponent;
using EntitiesSystem.EntityComponents.StatusEffectsComponent;
using EntitiesSystem.EntityComponents.TriggerComponent;
using ItemsSystem;
using SpritesSystem;
using StatusEffectsSystem;
using UnityEngine;

namespace EntitiesSystem
{
    public class EntityGenerator
    {

        #region Variables

        private readonly Game game;

        #endregion

        #region Constructor

        //TODO сменить зависимость от гейм менеджера на зависимость от других генераторов
        public EntityGenerator(Game _game)
        {
            game = _game;
        }

        #endregion

        #region Methods

        //TODO в актор лейер ставить тут или в другом месте? что если уровня не будет на этот момент
        //TODO может сменить привязку к визуалайзеру и лейеру на другое местог
        //TODO сделать что-то с порядком, т.к. статус эффекты зависят от актора
        //TODO сначала добавлять все нужные компоненты, потом уже инициализировать их
        //todo переписать чтобы все стало красиво и понятно, вообще может использовать префабы, а этот класс сделать скриптабл объектом
        public Entity GeneratePlayerActor()
        {
            GameObject playerGameObject = new GameObject("PlayerActor"); //Creating gameobject
            playerGameObject.transform.SetParent(game.GameGridStructure.ActorsLayer);

            Entity entity = playerGameObject.AddComponent<Entity>();
            playerGameObject.AddComponent<SpriteRenderer>();
            entity.SortingLayer = SortingLayers.Actors;
            entity.IsBlockingView = false;
            entity.IsCollidable = true;
            entity.SafeAddEntityComponent<CharacterAttributes>();
            entity.SafeAddEntityComponent<Inventory>();
            entity.SafeAddEntityComponent<PlayerActor>();
            entity.SafeAddEntityComponent<StatusEffectsSet>();
            entity.SafeAddEntityComponent<CustomAnimator>();
            entity.SafeAddEntityComponent<Player>();
            CustomAnimation idleAnimation = new CustomAnimation(AnimationName.Idle, 0.15f,
                game.SpriteManager.GetRandomHumanoidSprite());
            CustomAnimation walkAnimation = new CustomAnimation(AnimationName.Walk, 0.05f, idleAnimation.Frames);
            CustomAnimation hitAnimation = new CustomAnimation(AnimationName.Hit, 0.15f, idleAnimation.Frames);
            CustomAnimation attackAnimation = new CustomAnimation(AnimationName.Attack, 0.15f, idleAnimation.Frames);
            entity.CustomAnimator.Initialize(
                new CustomAnimation[4] { idleAnimation, walkAnimation, hitAnimation, attackAnimation }, game.SpriteManager.GetDefaultAnimationCurve(), 1.0f);
            entity.StatusEffectsSet.SubscribeToEvents();

            game.AttributesGenerator.GenerateAttributes(entity.CharacterAttributes, 1);
            for (int i = 0; i < 25; i++)
            {
                entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomEquippalbeItem());
            }
            entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomHealthPotionItem());
            entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomHealthPotionItem());
            entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomEmpowerPotionItem());
            entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomEmpowerPotionItem());

            entity.SafeAddEntityComponent<Aura>().Initialize(new Vector2Int[]
            {
                new Vector2Int(-1,1), new Vector2Int(0, 1), new Vector2Int(1, 1)
                ,new Vector2Int(-1,0),new Vector2Int(0,0),new Vector2Int(1,0)
                ,new Vector2Int(-1,-1),new Vector2Int(0,-1),new Vector2Int(1,-1)
            }, false);
            entity.HasEntityComponent<Aura>().SetStatusEffect(new EmpowerStatusEffect(game.SpriteManager.GetRandomScrollSprite(), 100,
                StatusEffectType.Beneficial));

            return entity;
        }

        public Entity GenerateCharacterActorWithPlayerActorController()
        {
            GameObject playerGameObject = new GameObject("CharacterUnderPlayerControl"); //Creating gameobject
            playerGameObject.transform.SetParent(game.GameGridStructure.ActorsLayer);

            Entity entity = playerGameObject.AddComponent<Entity>();
            playerGameObject.AddComponent<SpriteRenderer>();
            entity.SortingLayer = SortingLayers.Actors;
            entity.IsBlockingView = false;
            entity.IsCollidable = true;
            entity.SafeAddEntityComponent<CharacterAttributes>();
            entity.SafeAddEntityComponent<Inventory>();
            entity.SafeAddEntityComponent<PlayerActor>();
            entity.SafeAddEntityComponent<StatusEffectsSet>();
            entity.SafeAddEntityComponent<CustomAnimator>();
            entity.SafeAddEntityComponent<Character>();
            CustomAnimation idleAnimation = new CustomAnimation(AnimationName.Idle, 0.15f,
                game.SpriteManager.GetRandomHumanoidSprite());
            CustomAnimation walkAnimation = new CustomAnimation(AnimationName.Walk, 0.05f, idleAnimation.Frames);
            CustomAnimation hitAnimation = new CustomAnimation(AnimationName.Hit, 0.15f, idleAnimation.Frames);
            CustomAnimation attackAnimation = new CustomAnimation(AnimationName.Attack, 0.15f, idleAnimation.Frames);
            entity.CustomAnimator.Initialize(
                new CustomAnimation[4] { idleAnimation, walkAnimation, hitAnimation, attackAnimation }, game.SpriteManager.GetDefaultAnimationCurve(), 1.0f);

            entity.StatusEffectsSet.SubscribeToEvents();

            game.AttributesGenerator.GenerateAttributes(entity.CharacterAttributes, 1);
            for (int i = 0; i < 25; i++)
            {
                entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomEquippalbeItem());
            }

            return entity;
        }

        public Entity GenerateCharacterActor()
        {
            GameObject playerGameObject = new GameObject("EnemyActor"); //Creating gameobject
            playerGameObject.transform.SetParent(game.GameGridStructure.ActorsLayer);

            Entity entity = playerGameObject.AddComponent<Entity>();
            playerGameObject.AddComponent<SpriteRenderer>();
            entity.SortingLayer = SortingLayers.Actors;
            entity.IsBlockingView = false;
            entity.IsCollidable = true;

            entity.SafeAddEntityComponent<CharacterAttributes>();
            entity.SafeAddEntityComponent<Inventory>();
            entity.SafeAddEntityComponent<NpcActor>();
            entity.SafeAddEntityComponent<StatusEffectsSet>();
            entity.SafeAddEntityComponent<CustomAnimator>();
            entity.SafeAddEntityComponent<Character>();

            CustomAnimation idleAnimation = new CustomAnimation(AnimationName.Idle, 0.15f,
                game.SpriteManager.GetRandomHumanoidSprite());
            CustomAnimation walkAnimation = new CustomAnimation(AnimationName.Walk, 0.05f, idleAnimation.Frames);
            CustomAnimation hitAnimation = new CustomAnimation(AnimationName.Hit, 0.15f, idleAnimation.Frames);
            CustomAnimation attackAnimation = new CustomAnimation(AnimationName.Attack, 0.15f, idleAnimation.Frames);
            entity.CustomAnimator.Initialize(
                new CustomAnimation[4] { idleAnimation, walkAnimation, hitAnimation, attackAnimation }, game.SpriteManager.GetDefaultAnimationCurve(), 1.0f);

            entity.StatusEffectsSet.SubscribeToEvents();


            game.AttributesGenerator.GenerateAttributes(entity.CharacterAttributes, 1);
            for (int i = 0; i < 25; i++)
            {
                entity.Inventory.AddItem(game.ItemsGenerator.GenerateRandomEquippalbeItem());
            }

            return entity;
        }

        public Entity GenerateItemEntityForAbstractItem(AbstractItem _abstractItem)
        {
            Entity item = CreateGameObjectForEntity(game.GameGridStructure.ItemsLayer);
            item.SortingLayer = SortingLayers.Items;
            item.IsBlockingView = false;
            item.IsCollidable = false;
            item.SafeAddEntityComponent<Item>();
            item.Item.AbstractItem = _abstractItem;
            return item;
        }

        public Entity GenerateDoorEntity(Sprite[] _doorOpenedClosedSprites)
        {
            Entity door = CreateGameObjectForEntity(game.GameGridStructure.EntitiesLayer);
            door.Name = "door";
            door.SortingLayer = SortingLayers.Entities;
            door.SafeAddEntityComponent<DoorInteractable>().Initialize(_doorOpenedClosedSprites, true);
            return door;
        }

        public Entity GenerateTestTriggerEntity()
        {
            Entity triggerEntity = CreateGameObjectForEntity(game.GameGridStructure.EntitiesLayer);
            triggerEntity.SortingLayer = SortingLayers.Entities;
            triggerEntity.IsBlockingView = false;
            triggerEntity.IsCollidable = false;
            triggerEntity.Name = "test trigger";
            triggerEntity.SafeAddEntityComponent<Trap>().Initialize(new Vector2Int[]
            {
                new Vector2Int(-1,1), new Vector2Int(0, 1), new Vector2Int(1, 1)
                ,new Vector2Int(-1,0),new Vector2Int(0,0),new Vector2Int(1,0)
                ,new Vector2Int(-1,-1),new Vector2Int(0,-1),new Vector2Int(1,-1)
            }, true);
            triggerEntity.SpriteRenderer.sprite = game.SpriteManager.GetRandomEffectSprite();
            return triggerEntity;
        }

        private Entity CreateGameObjectForEntity(Transform _parentTransform, params Type[] _components)
        {
            GameObject gameObject = new GameObject(typeof(Entity).ToString(), _components);
            gameObject.transform.SetParent(_parentTransform);
            return gameObject.AddComponent<Entity>();
        }

        #endregion

    }
}
