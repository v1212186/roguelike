﻿using System;
using System.Collections.Generic;
using EntitiesSystem.EntityComponents;
using EntitiesSystem.EntityComponents.ActorComponent;
using EntitiesSystem.EntityComponents.CharacterAttributesComponent;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using EntitiesSystem.EntityComponents.InteractableComponent;
using EntitiesSystem.EntityComponents.InventoryComponent;
using EntitiesSystem.EntityComponents.ItemComponent;
using EntitiesSystem.EntityComponents.StatusEffectsComponent;
using EntitiesSystem.EntityComponents.TriggerComponent;
using LevelsSystem;
using SpritesSystem;
using UnityEngine;
using Utilities;

namespace EntitiesSystem
{
    //TODO может возвращать список экшонов если клетка с энтити занята?
    //TODO или новый компонент - интерактабл, то есть если есть компонент, то можем интерактить и возвращаем экшон по ситуации
    [DisallowMultipleComponent]
    public class Entity : MonoBehaviour
    {

        #region Variables
        [SerializeField]
        private string entityName;
        public string Name
        {
            get { return entityName; }
            set
            {
                entityName = value;
                gameObject.name = entityName;
            }
        }

        [SerializeField]
        private bool isCollidable;
        public bool IsCollidable
        {
            get { return isCollidable; }
            set { isCollidable = value; }
        }

        [SerializeField]
        private bool isBlockingView;
        public bool IsBlockingView
        {
            get { return isBlockingView; }
            set { isBlockingView = value; }
        }

        [SerializeField]
        private SortingLayers sortingLayer;
        public SortingLayers SortingLayer
        {
            get { return sortingLayer; }
            set
            {
                sortingLayer = value;
                SpriteRenderer.sortingLayerName = sortingLayer.ToString();
            }
        }

        [SerializeField]
        private Vector2Int position;
        public Vector2Int Position { get { return position; } }
        private Level level;
        public Level Level { get { return level; } }

        #region Entity components
        [SerializeField]
        protected SpriteRenderer spriteRenderer;
        public SpriteRenderer SpriteRenderer
        {
            get
            {
                if (spriteRenderer != null) return spriteRenderer;
                spriteRenderer = GetComponent<SpriteRenderer>();
                if (spriteRenderer == null)
                {
                    spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                }
                return spriteRenderer;
            }
        }

        [SerializeField]
        protected Inventory inventory;
        public Inventory Inventory
        {
            get
            {
                if (inventory == null)
                {
                    inventory = GetComponent<Inventory>();
                }
                return inventory;
            }
        }
        [SerializeField]
        protected CharacterAttributes characterAttributes;
        public CharacterAttributes CharacterAttributes
        {
            get
            {
                if (characterAttributes == null)
                {
                    characterAttributes = GetComponent<CharacterAttributes>();
                }
                return characterAttributes;
            }
        }

        [SerializeField]
        protected Actor actor;
        public Actor Actor
        {
            get
            {
                if (actor == null)
                {
                    actor = GetComponent<Actor>();
                }
                return actor;
            }
        }

        [SerializeField]
        protected SimpleActor simpleActor;
        public SimpleActor SimpleActor
        {
            get
            {
                if (simpleActor == null)
                {
                    simpleActor = GetComponent<SimpleActor>();
                }
                return simpleActor;
            }
        }

        [SerializeField]
        protected StatusEffectsSet statusEffectsSet;
        public StatusEffectsSet StatusEffectsSet
        {
            get
            {
                if (statusEffectsSet == null)
                {
                    statusEffectsSet = GetComponent<StatusEffectsSet>();
                }
                return statusEffectsSet;
            }
        }

        [SerializeField]
        protected CustomAnimator customAnimator;
        public CustomAnimator CustomAnimator
        {
            get
            {
                if (customAnimator == null)
                {
                    customAnimator = GetComponent<CustomAnimator>();
                }
                return customAnimator;
            }
        }

        [SerializeField]
        protected Item item;
        public Item Item
        {
            get
            {
                if (item == null)
                {
                    item = GetComponent<Item>();
                }
                return item;
            }
        }

        [SerializeField]
        protected Interactable interactable;
        public Interactable Interactable
        {
            get
            {
                if (interactable == null)
                {
                    interactable = GetComponent<Interactable>();
                }
                return interactable;
            }
        }

        [SerializeField]
        protected Trigger trigger;
        public Trigger Trigger
        {
            get
            {
                if (trigger == null)
                {
                    trigger = GetComponent<Trigger>();
                }
                return trigger;
            }
        }

        private readonly List<EntityComponent> entityComponents = new List<EntityComponent>();
        #endregion

        #region Events

        public readonly GameEvent<Entity, ValueChangeEventArgs<Vector2Int>> OnEntityPositionChanged = new GameEvent<Entity, ValueChangeEventArgs<Vector2Int>>();

        public readonly GameEvent<Entity, LevelChange> OnEntityPlacedOnLevel = new GameEvent<Entity, LevelChange>();

        public readonly GameEvent<Entity, EventArgs> OnEntityDestroyed = new GameEvent<Entity, EventArgs>();

        #endregion

        #endregion

        #region Methods

        public void PlaceEntityOnLevel(Level _level, Vector2Int _position)
        {
            level = _level;
            level.RegisterEntityOnLevel(this);
            transform.localPosition = new Vector3(_position.x, _position.y, 0f);
            UpdatePosition(_position);
            OnEntityPlacedOnLevel.Raise(this, new LevelChange(null, _level));
        }

        public void UpdatePosition(Vector2Int _newPosition)
        {
            Vector2Int oldPosition = position;
            level.UnregisterEntityFromCell(this, oldPosition);
            position = _newPosition;
            level.RegisterEntityOnCell(this, position);
            OnEntityPositionChanged.Raise(this, new ValueChangeEventArgs<Vector2Int>(oldPosition, position));
        }

        public T SafeAddEntityComponent<T>() where T : EntityComponent
        {
            EntityComponent entityComponent = HasEntityComponent<T>();
            if (entityComponent != null)
            {
                return entityComponent as T;
            }
            entityComponent = this.gameObject.AddComponent<T>();
            entityComponents.Add(entityComponent);
            return entityComponent as T;
        }

        public T HasEntityComponent<T>() where T : EntityComponent
        {
            for (int i = 0; i < entityComponents.Count; i++)
            {
                if (entityComponents[i].GetType() == typeof(T))
                {
                    return entityComponents[i] as T;
                }
            }
            EntityComponent entityComponent = GetComponent<T>();
            if (entityComponent != null)
            {
                entityComponents.Add(entityComponent);
            }
            return entityComponent as T;
        }

        //TODO temp, наверное не через дестрой делать, а через менеджер, типо DisposeEntity()
        public void DestroyEntity()
        {
            level.UnregisterEntityFromCell(this, position);
            level.UnregisterEntityFromLevel(this);
            OnEntityDestroyed.Raise(this, EventArgs.Empty);
            Destroy(gameObject);
        }

        #endregion
    }
}
