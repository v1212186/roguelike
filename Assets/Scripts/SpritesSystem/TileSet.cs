﻿using System;
using LevelsSystem;
using UnityEngine;

namespace SpritesSystem
{
    [Serializable]
    public class TileSet
    {
        [SerializeField]
        private DynamicTile floorTile;
        public DynamicTile FloorTile { get { return floorTile; } }
        [SerializeField]
        private DynamicTile wallTile;
        public DynamicTile WallTile { get { return wallTile; } }
        [SerializeField]
        private Sprite[] doorOpenedClosedSprites;
        public Sprite[] DoorOpenedClosedSprites { get { return doorOpenedClosedSprites; } }
    }
}
