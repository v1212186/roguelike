﻿using System.Collections.Generic;
using ItemsSystem;
using UnityEngine;

namespace SpritesSystem
{
    public class SpriteManager : ScriptableObject
    {
        #region Variables
        [SerializeField]
        private Sprite[] weaponsSprites;
        [SerializeField]
        private Sprite[] bootsSprites;
        [SerializeField]
        private Sprite[] armorSprites;
        [SerializeField]
        private Sprite[] helmetSprites;
        [SerializeField]
        private Sprite[] glovesSprites;
        [SerializeField]
        private Sprite[] scrollsSprites;
        [SerializeField]
        private Sprite[] potionSprites;
        [SerializeField]
        private List<TileSet> tileSets;
        [SerializeField]
        private Sprite[] humanoid0Sprites;
        [SerializeField]
        private Sprite[] humanoid1Sprites;
        [SerializeField]
        private Sprite[] effectsSprites;
        [SerializeField]
        private AnimationCurve animationCurve;
        #endregion

        #region Methods
        public Sprite GetRandomEquippableItemSpriteBySlot(EquipmentSlot _equipmentSlot)
        {
            switch (_equipmentSlot)
            {
                case EquipmentSlot.Armor:
                    return armorSprites[Random.Range(0, armorSprites.Length)];
                case EquipmentSlot.Boots:
                    return bootsSprites[Random.Range(0, bootsSprites.Length)];
                case EquipmentSlot.Helmet:
                    return helmetSprites[Random.Range(0, helmetSprites.Length)];
                case EquipmentSlot.Gloves:
                    return glovesSprites[Random.Range(0, glovesSprites.Length)];
                case EquipmentSlot.Weapon:
                    return weaponsSprites[Random.Range(0, weaponsSprites.Length)];
                default:
                    return null;
            }
        }

        public Sprite GetRandomScrollSprite()
        {
            return scrollsSprites[Random.Range(0, scrollsSprites.Length)];
        }

        public Sprite GetRandomPotionSprite()
        {
            return potionSprites[Random.Range(0, potionSprites.Length)];
        }

        public TileSet GetRandomTileSet()
        {
            return tileSets[Random.Range(0, tileSets.Count)];
        }

        public Sprite[] GetRandomHumanoidSprite()
        {
            Sprite[] sprites = new Sprite[2];
            int rng = Random.Range(0, humanoid0Sprites.Length);
            sprites[0] = humanoid0Sprites[rng];
            sprites[1] = humanoid1Sprites[rng];
            return sprites;
        }

        public Sprite GetRandomEffectSprite()
        {
            return effectsSprites[Random.Range(0, effectsSprites.Length)];
        }

        public AnimationCurve GetDefaultAnimationCurve()
        {
            return animationCurve;
        }
        #endregion


    }
}
