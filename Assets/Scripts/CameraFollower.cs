﻿using System;
using Cinemachine;
using EntitiesSystem;
using LevelsSystem;
using UnityEngine;

public class CameraFollower : MonoBehaviour
{
    //TODO хранить за кем следить в стеке, первым - игрок, на остальных фокус переключать по интерфейсу
    #region Variables

    [SerializeField]
    private CinemachineVirtualCamera cinemachineVirtualCamera;
    [SerializeField]
    private GameObject followTarget;

    #endregion

    #region Monobehaviour methods
#if UNITY_EDITOR
    private void OnValidate()
    {
        cinemachineVirtualCamera = FindObjectOfType<CinemachineVirtualCamera>();
        if (followTarget == null)
        {
            Debug.LogError("No GameObject [FollowTarget] reference at: " + this);
        }
    }
#endif

    private void Start()
    {
        cinemachineVirtualCamera.Follow = followTarget.transform;
    }
    #endregion

    #region Methods

    public void SubscribeToEvents(Game _game)
    {
        _game.OnLevelChanged.Add(OnLevelChanged);
    }

    private void OnLevelChanged(object _o, LevelChange _levelChange)
    {
        if (_levelChange.PreviousLevel!= null)
        {
            _levelChange.PreviousLevel.GameLoop.OnPlayerActorChanged.Remove(OnPlayerActorChanged);
        }
        _levelChange.CurrentLevel.GameLoop.OnPlayerActorChanged.Add(OnPlayerActorChanged);
    }

    private void OnPlayerActorChanged(object _o, ActorChanged _actorChanged)
    {
        OnCurrentPlayerActorChanged(_actorChanged.previousActorEntity, _actorChanged.currentActorEntity);
    }

    public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
    {
        followTarget.transform.SetParent(_currentEntity.transform);
        followTarget.transform.localPosition = Vector3.zero;
    }
    #endregion


}
