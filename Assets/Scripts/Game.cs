﻿using System;
using AttributesSystem;
using EntitiesSystem;
using ItemsSystem;
using LevelsSystem;
using SpritesSystem;
using StateMachineSystem;
using StateMachineSystem.GameManagerStates;
using UnityEngine;
using UnityEngine.Tilemaps;
using UserInterface;
using Utilities;

[Serializable]
public class GameGridStructure
{
    [SerializeField]
    private Tilemap tilemap;
    public Tilemap Tilemap { get { return tilemap; } }
    [SerializeField]
    private Transform tilesLayer;
    public Transform TilesLayer { get { return tilesLayer; } }
    [SerializeField]
    private Transform entitiesLayer;
    public Transform EntitiesLayer { get { return entitiesLayer; } }
    [SerializeField]
    private Transform itemsLayer;
    public Transform ItemsLayer { get { return itemsLayer; } }
    [SerializeField]
    private Transform actorsLayer;
    public Transform ActorsLayer { get { return actorsLayer; } }
    [SerializeField]
    private Transform effectsLayer;
    public Transform EffectsLayer { get { return effectsLayer; } }
}

public class LevelChange : EventArgs
{
    #region Variables

    public readonly Level PreviousLevel;
    public readonly Level CurrentLevel;

    #endregion

    #region Contructor

    public LevelChange(Level _previousLevel, Level _currentLevel)
    {
        PreviousLevel = _previousLevel;
        CurrentLevel = _currentLevel;
    }

    #endregion
}

public class Game : MonoBehaviour
{
    #region Variables

    private Level currentLevel;
    public Level CurrentLevel
    {
        get { return currentLevel; }
        set
        {
            Level previousLevel = currentLevel;
            currentLevel = value;
            OnLevelChanged.Raise(this, new LevelChange(previousLevel, currentLevel));
        }
    }

    [SerializeField]
    private GameGridStructure gameGridStructure;
    public GameGridStructure GameGridStructure { get { return gameGridStructure; } }

    [SerializeField]
    private UserInterfaceManager userInterfaceManager;
    public UserInterfaceManager UserInterfaceManager { get { return userInterfaceManager; } }

    [SerializeField]
    private CameraFollower cameraFollower;
    public CameraFollower CameraFollower { get { return cameraFollower; } }

    [SerializeField]
    private SpriteManager spriteManager;
    public SpriteManager SpriteManager { get { return spriteManager; } }

    [SerializeField]
    private AttributesGenerator attributesGenerator;
    public AttributesGenerator AttributesGenerator { get { return attributesGenerator; } }

    [SerializeField]
    private ItemsGenerator itemsGenerator;
    public ItemsGenerator ItemsGenerator { get { return itemsGenerator; } }

    [SerializeField]
    private LevelGenerator levelGenerator;
    public LevelGenerator LevelGenerator { get { return levelGenerator; } }

    [SerializeField]
    private LevelVizualizer levelVizualizer;
    public LevelVizualizer LevelVizualizer { get { return levelVizualizer; } }

    [SerializeField]
    private EntityGenerator entityGenerator;
    public EntityGenerator EntityGenerator { get { return entityGenerator; } }

    #region Events

    public readonly GameEvent<object, LevelChange> OnLevelChanged = new GameEvent<object, LevelChange>();

    #endregion

    #region StateMachine and states
    private StateMachine stateMachine;
    public StateMachine StateMachine { get { return stateMachine; } }

    private InitializeState initializeState;
    private MainMenuState mainMenuState;
    public MainMenuState MainMenuState { get { return mainMenuState; } }
    private GeneratingLevelState generatingLevelState;
    public GeneratingLevelState GeneratingLevelState { get { return generatingLevelState; } }
    private PlacingEntitiesState placingEntitiesState;
    public PlacingEntitiesState PlacingEntitiesState { get { return placingEntitiesState; } }
    private ProcessingCurrentLevelState processingCurrentLevelState;
    public ProcessingCurrentLevelState ProcessingCurrentLevelState { get { return processingCurrentLevelState; } }
    #endregion

    #endregion

    #region Monobehaviour methods

#if UNITY_EDITOR
    private void OnValidate()
    {
        userInterfaceManager = FindObjectOfType<UserInterfaceManager>();
        cameraFollower = FindObjectOfType<CameraFollower>();
        if (spriteManager == null)
        {
            Debug.LogError("No [SpriteManager] reference at: " + this);
        }
    }
#endif

    private void Start()
    {
        //TODO саббить тут те что он левел чейнджед?
        levelGenerator = new LevelGenerator(this);
        levelVizualizer = new LevelVizualizer(gameGridStructure.Tilemap);
        entityGenerator = new EntityGenerator(this);
        attributesGenerator = new AttributesGenerator();
        itemsGenerator = new ItemsGenerator(this);
        stateMachine = new StateMachine();
        initializeState = new InitializeState(this);
        mainMenuState = new MainMenuState(this);
        generatingLevelState = new GeneratingLevelState(this);
        placingEntitiesState = new PlacingEntitiesState(this);
        processingCurrentLevelState = new ProcessingCurrentLevelState(this);
        stateMachine.SetState(initializeState);
    }

    private void Update()
    {
        stateMachine.ProcessCurrentState();
    }

    #endregion

    #region Methods

    public void StartGame()
    {
        stateMachine.SetState(GeneratingLevelState);
    }

    #endregion

}
