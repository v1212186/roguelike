﻿using AttributesSystem;
using EntitiesSystem;
using UnityEngine;

namespace StatusEffectsSystem
{
    public class EmpowerStatusEffect : AbstractStatusEffect
    {

        #region Varibles

        private AttributeModifier attributeModifier;

        #endregion

        #region Constructor
        public EmpowerStatusEffect(Sprite _sprite, int _duration, StatusEffectType _statusEffectType) : base(_sprite, _duration, _statusEffectType)
        {
            attributeModifier = new AttributeModifier(50.0f, AttributeModifierType.Flat, AttributeType.Attack, this);
        }
        #endregion

        #region Methods

        public override bool ApplyStatusEffect(Entity _entity)
        {
            _entity.CharacterAttributes.AddAttributeModifier(attributeModifier);
            return true;
        }

        public override bool RemoveStatusEffect(Entity _entity)
        {
            return _entity.CharacterAttributes.RemoveAllModifiersFromSource(this);
        }

        public override string GetDescription()
        {
            return "Modify [" + attributeModifier.AttributeType + "] for value [" + attributeModifier.Value+"]";
        }

        #endregion
    }
}
