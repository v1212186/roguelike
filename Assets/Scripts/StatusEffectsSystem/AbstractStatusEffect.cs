﻿using EntitiesSystem;
using UnityEngine;

namespace StatusEffectsSystem
{
    public enum StatusEffectType
    {
        Harmful, Beneficial
    }

    //TODO сделать поддержку бесконечных статус эффектов
    //todo добавить энум: временный, бесконечный, мгновенный
    //TODO добавить обновление времени эффекта при наложении такого же эффекта
    public abstract class AbstractStatusEffect
    {
        #region Variables

        public readonly Sprite Sprite;
        public readonly int Duration;
        public readonly StatusEffectType statusEffectType;
        protected int currentDuration;
        public int CurrentDuration { get { return currentDuration; } }
        protected object source;
        public object Source { get { return source; } }

        #endregion

        #region Constructor

        protected AbstractStatusEffect(Sprite _sprite, int _duration, StatusEffectType _statusEffectType)
        {
            Sprite = _sprite;
            Duration = _duration;
            statusEffectType = _statusEffectType;
            currentDuration = Duration;
        }

        #endregion

        #region Methods

        public void SetSource(object _source)
        {
            source = _source;
        }

        public virtual int ChangeDuration(int _value)
        {
            return currentDuration = (int)Mathf.Clamp(currentDuration + _value, 0.0f, Duration);
        }

        public abstract bool ApplyStatusEffect(Entity _entity);

        public abstract bool RemoveStatusEffect(Entity _entity);

        public abstract string GetDescription();

        #endregion
    }
}
