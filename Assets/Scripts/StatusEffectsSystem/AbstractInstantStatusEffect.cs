﻿using UnityEngine;

namespace StatusEffectsSystem
{
    public abstract class AbstractInstantStatusEffect : AbstractStatusEffect
    {

        #region Constructor
        protected AbstractInstantStatusEffect(Sprite _sprite, StatusEffectType _statusEffectType) : base(_sprite, 0, _statusEffectType)
        {
        }
        #endregion

        #region Methods
     
        #endregion
    }
}
