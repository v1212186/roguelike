﻿using EntitiesSystem;
using UnityEngine;

namespace StatusEffectsSystem
{
    public class ReceiveHealStatusEffect : AbstractInstantStatusEffect
    {

        #region Variables

        private int healthRestored;

        #endregion

        #region Constructor
        public ReceiveHealStatusEffect(Sprite _sprite, StatusEffectType _statusEffectType, int _healthAmount) : base(_sprite, _statusEffectType)
        {
            healthRestored = _healthAmount;
        }
        #endregion

        #region Methods
        public override bool ApplyStatusEffect(Entity _entity)
        {
            _entity.CharacterAttributes.HealthAttribute.ChangeCurrentValue(healthRestored);
            return true;
        }

        public override bool RemoveStatusEffect(Entity _entity)
        {
            return true;
        }

        public override string GetDescription()
        {
            return "Restore [health] for value [" + healthRestored + "]";
        }

        #endregion
    }
}
