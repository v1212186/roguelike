﻿using System;
using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface;
using UserInterface.MainMenuInterface;

namespace UserInterface
{
    public class UserInterfaceManager : MonoBehaviour
    {
        #region Variables

        [SerializeField]
        private MainMenuPanel mainMenuPanel;
        public MainMenuPanel MainMenuPanel { get { return mainMenuPanel; } }
        [SerializeField]
        private InGamePanel inGamePanel;
        public InGamePanel InGamePanel { get { return inGamePanel; } }

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (mainMenuPanel == null)
            {
                Debug.LogError("Missing [MainMenuPanel] reference at " + this);
            }
            if (inGamePanel == null)
            {
                Debug.LogError("Missing [InGamePanel] reference at " + this);
            }
        }
#endif

        #endregion

        #region Methods

        public void SubscribeToEvents(Game _game)
        {
            _game.OnLevelChanged.Add(OnLevelChanged);
        }

        private void OnLevelChanged(object _o, LevelChange _levelChange)
        {
            inGamePanel.OnLevelChanged(_levelChange.PreviousLevel, _levelChange.CurrentLevel);
        }

        #endregion


    }
}
