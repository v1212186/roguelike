﻿using StatusEffectsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.InGameInterface.StatusEffectsInterface
{
    public class StatusEffectIndicator : MonoBehaviour
    {
        #region Variables

        [SerializeField]
        private Image image;

        private AbstractStatusEffect abstractStatusEffect;

        public AbstractStatusEffect AbstractStatusEffect
        {
            get { return abstractStatusEffect; }
            set
            {
                abstractStatusEffect = value;
                image.sprite = abstractStatusEffect.Sprite;
                //TODO числа для отсчета
            }
        }

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            image = GetComponent<Image>();
        }
#endif
        #endregion


    }
}
