﻿using System;
using System.Collections.Generic;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.StatusEffectsComponent;
using LevelsSystem;
using UnityEngine;

namespace UserInterface.InGameInterface.StatusEffectsInterface
{
    //TODO возможно разделить отрицательные и положительные эффекты на 2 полосы
    //TODO что если полоса не будет влезать в экран
    //TODO сделать общий базовый класс для панелей
    public class StatusEffectsInGamePanel : MonoBehaviour
    {

        #region Variables
        [SerializeField]
        private StatusEffectIndicator statusEffectIndicatorPrefab;
        [SerializeField]
        private Transform statusEffectsGrid;
        [SerializeField]
        private List<StatusEffectIndicator> statusEffectIndicators = new List<StatusEffectIndicator>();
        [SerializeField]
        private StatusEffectsSet currentStatusEffectsSet;
        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (statusEffectIndicatorPrefab == null)
            {
                Debug.LogError("No [StatusEffectTooltipPrefab] at: " + this);
            }
            statusEffectsGrid = GetComponent<RectTransform>();
        }
#endif

        #endregion

        #region Methods

        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentStatusEffectsSet(_currentEntity.StatusEffectsSet);
        }

        private void SetCurrentStatusEffectsSet(StatusEffectsSet _statusEffectsSet)
        {
            if (currentStatusEffectsSet != null)
            {
                currentStatusEffectsSet.OnStatusEffectsChanged.Remove(OnStatusEffectsChanged);
            }
            currentStatusEffectsSet = _statusEffectsSet;
            currentStatusEffectsSet.OnStatusEffectsChanged.Add(OnStatusEffectsChanged);
            RefreshStatusEffectsPanel();
        }

        private void OnStatusEffectsChanged(Entity _entity, StatusEffectChange _statusEffectChange)
        {
            RefreshStatusEffectsPanel();
        }

        private void RefreshStatusEffectsPanel()
        {
            for (int i = statusEffectIndicators.Count - 1; i >= 0; i--)
            {
                Destroy(statusEffectIndicators[i].gameObject);
                statusEffectIndicators.RemoveAt(i);
            }
            for (int i = 0; i < currentStatusEffectsSet.StatusEffects.Count; i++)
            {
                StatusEffectIndicator statusEffectIndicator =
                    Instantiate(statusEffectIndicatorPrefab.gameObject, statusEffectsGrid)
                        .GetComponent<StatusEffectIndicator>();
                statusEffectIndicator.AbstractStatusEffect = currentStatusEffectsSet.StatusEffects[i];
                statusEffectIndicators.Add(statusEffectIndicator);
            }
        }

        #endregion

    }
}
