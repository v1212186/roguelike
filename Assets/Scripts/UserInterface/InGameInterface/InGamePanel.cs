﻿using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface.EntityStatsInterface;
using UserInterface.InGameInterface.HealthBarInterface;
using UserInterface.InGameInterface.ItemPickupInterface;
using UserInterface.InGameInterface.StatusEffectsInterface;

namespace UserInterface.InGameInterface
{
    public class InGamePanel : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        private GameObject showEntityStatsButton;
        [SerializeField]
        private StatusEffectsInGamePanel statusEffectsInGamePanel;
        [SerializeField]
        private EntityStatsPanel entityStatsPanel;
        [SerializeField]
        private HealthBarPanel healthBarPanel;
        [SerializeField]
        private ItemPickupPanel itemPickupPanel;
        #endregion

        #region Monobehaviour methods


#if UNITY_EDITOR
        private void OnValidate()
        {
            if (showEntityStatsButton == null)
            {
                Debug.LogError("Missing [ShowEntityStatsButton] reference at " + this);
            }
            statusEffectsInGamePanel = GetComponentInChildren<StatusEffectsInGamePanel>(true);
            healthBarPanel = GetComponentInChildren<HealthBarPanel>(true);
            entityStatsPanel = GetComponentInChildren<EntityStatsPanel>(true);
            itemPickupPanel = GetComponentInChildren<ItemPickupPanel>(true);
        }
#endif

        #endregion

        #region Methods

        public void OnLevelChanged(Level _previousLevel, Level _currentLevel)
        {
            if (_previousLevel != null)
            {
                _previousLevel.GameLoop.OnPlayerActorChanged.Remove(OnPlayerActorChanged);
            }
            _currentLevel.GameLoop.OnPlayerActorChanged.Add(OnPlayerActorChanged);

            itemPickupPanel.OnLevelChanged(_previousLevel, _currentLevel);
        }

        private void OnPlayerActorChanged(object _o, ActorChanged _actorChanged)
        {
            statusEffectsInGamePanel.OnCurrentPlayerActorChanged(_actorChanged.previousActorEntity, _actorChanged.currentActorEntity);
            healthBarPanel.OnCurrentPlayerActorChanged(_actorChanged.previousActorEntity, _actorChanged.currentActorEntity);
            entityStatsPanel.OnCurrentPlayerActorChanged(_actorChanged.previousActorEntity, _actorChanged.currentActorEntity);
            itemPickupPanel.OnCurrentPlayerActorChanged(_actorChanged.previousActorEntity, _actorChanged.currentActorEntity);
        }

        public void ShowInGamePanel()
        {
            gameObject.SetActive(true);
        }

        public void CloseInGamePanel()
        {
            gameObject.SetActive(false);
        }

        #endregion


    }
}
