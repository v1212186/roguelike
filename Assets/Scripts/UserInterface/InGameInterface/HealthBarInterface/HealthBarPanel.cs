﻿using AttributesSystem;
using EntitiesSystem;
using LevelsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.InGameInterface.HealthBarInterface
{
    public class HealthBarPanel : MonoBehaviour
    {
        //TODO наверное эвент кью для получения урона и плавной анимацции

        #region Variables

        [SerializeField]
        private RectTransform healthBarRectTransform;
        private HealthAttribute currentHealthAttribute;
        [SerializeField]
        private RectTransform healthBarImageRectTransform;
        [SerializeField]
        private Text healthText;
        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            healthBarRectTransform = GetComponent<RectTransform>();
            healthText = GetComponentInChildren<Text>();
        }
#endif

        //TODO temp, потом переделать с анимациями и подпиской на методы онхелсчейджед
        private void Update()
        {
            if (currentHealthAttribute == null)
            {
                return;
            }
            RefreshStatusEffectsPanel();
        }

        #endregion

        #region Methods


        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentHealthAttribute(_currentEntity.CharacterAttributes.HealthAttribute);
        }

        public void SetCurrentHealthAttribute(HealthAttribute _healthAttribute)
        {
            currentHealthAttribute = _healthAttribute;
        }

        public void RefreshStatusEffectsPanel()
        {
            healthBarImageRectTransform.sizeDelta = new Vector2(healthBarRectTransform.sizeDelta.x *
                (currentHealthAttribute.CurrentValue / currentHealthAttribute.FinalValue) - healthBarRectTransform.sizeDelta.x
                , healthBarImageRectTransform.sizeDelta.y);
            healthText.text = currentHealthAttribute.CurrentValue + "/" + currentHealthAttribute.FinalValue;
        }

        #endregion
       
    }
}
