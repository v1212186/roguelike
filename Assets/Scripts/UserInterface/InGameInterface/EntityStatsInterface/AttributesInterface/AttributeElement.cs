﻿using AttributesSystem;
using UnityEngine;
using UnityEngine.UI;
using UserInterface.InGameInterface.InteractableInterfaceElement;

namespace UserInterface.InGameInterface.EntityStatsInterface.AttributesInterface
{
    public class AttributeElement : InteractableElement
    {
        //TODO сделать по аналогии с абстракт итем слотом, то есть добавить в АттрибутсПанель добавить подписку на ивенты
        #region Variables
        [SerializeField]
        private AttributeType attributeType;
        public AttributeType AttributeType { get { return attributeType; } }
        [SerializeField]
        private Text attributeName;
        [SerializeField]
        private Text attributeValue;

        #endregion

        #region Monobehaviour methods
#if UNITY_EDITOR
        private void OnValidate()
        {
            gameObject.name = attributeType.ToString() + "Attribute";
            attributeName.text = attributeType.ToString();
        }
#endif
        #endregion

        #region Methods

        public void UpdateAttributeValue(float _value)
        {
            attributeValue.text = _value.ToString("0.00");
        }

        public override void OnShortPress()
        {
            throw new System.NotImplementedException();
        }

        public override void OnLongPress()
        {
            throw new System.NotImplementedException();
        }

        #endregion


    }
}
