﻿using System;
using AttributesSystem;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.CharacterAttributesComponent;
using LevelsSystem;
using UnityEngine;

namespace UserInterface.InGameInterface.EntityStatsInterface.AttributesInterface
{
    public class AttributesPanel : MonoBehaviour
    {

        #region Variables

        [SerializeField]
        private CharacterAttributes currentCharacterAttributes;
        [SerializeField]
        private AttributeElement[] attributeElements;

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            attributeElements = GetComponentsInChildren<AttributeElement>();
        }

#endif
        #endregion

        #region Methods

        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentCharacterAttributes(_currentEntity.CharacterAttributes);
        }

        private void SetCurrentCharacterAttributes(CharacterAttributes _characterAttributes)
        {
            if (currentCharacterAttributes != null)
            {
                currentCharacterAttributes.OnCharacterAttributesChanged.Remove(OnCharacterAttributesChanged);
            }
            currentCharacterAttributes = _characterAttributes;
            currentCharacterAttributes.OnCharacterAttributesChanged.Add(OnCharacterAttributesChanged);
            RefreshAttributesPanel();
        }

        private void OnCharacterAttributesChanged(Entity _entity, EventArgs _eventArgs)
        {
            RefreshAttributesPanel();
        }

        public void ShowAttributesPanel()
        {
            gameObject.SetActive(true);

        }

        public void CloseAttributesPanel()
        {
            gameObject.SetActive(false);
        }

        private void RefreshAttributesPanel()
        {
            for (int i = 0; i < attributeElements.Length; i++)
            {
                switch (attributeElements[i].AttributeType)
                {
                    case AttributeType.Health:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.HealthAttribute.FinalValue);
                        break;
                    case AttributeType.Energy:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.EnergyAttribute.FinalValue);
                        break;
                    case AttributeType.Attack:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.AttackAttribute.FinalValue);
                        break;
                    case AttributeType.Defence:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.DefenceAttribute.FinalValue);
                        break;
                    case AttributeType.CriticalHit:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.CriticalHitChanceAttribute.FinalValue);
                        break;
                    case AttributeType.HitChance:
                        attributeElements[i].UpdateAttributeValue(currentCharacterAttributes.HitChanceAttribute.FinalValue);
                        break;
                }
            }
        }

        #endregion


    }
}
