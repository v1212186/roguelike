﻿using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.InGameInterface.EntityStatsInterface
{
    public class ShowEntityStatsButton : MonoBehaviour
    {
        #region Variables

        [SerializeField]
        private Button button;
        [SerializeField]
        private EntityStatsPanel entityStatsPanel;

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            button = GetComponent<Button>();
        }
#endif

        private void Start()
        {
            button.onClick.AddListener(ShowEntityStatsPanel);
        }

        #endregion

        #region Methods

        private void ShowEntityStatsPanel()
        {
            entityStatsPanel.ShowEntityStatsPanel();
        }

        #endregion
    }
}
