﻿using System;
using ActionsSystem;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.InventoryComponent;
using ItemsSystem;
using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface.AbstractItemTooltipInterface;

namespace UserInterface.InGameInterface.EntityStatsInterface.InventoryInterface
{
    //TODO вместо инвентаря хранить текущий энтити
    public class InventoryPanel : MonoBehaviour, IOnAbstractItemSlotShortPress
    {
        #region Variables
        [SerializeField]
        private Transform itemSlotsGrid;
        [SerializeField]
        private AbstractItemSlot[] itemSlots;
        [SerializeField]
        private Inventory currentInventory;
        [SerializeField]
        private AbstractItemTooltip abstractItemTooltip;
        #endregion

        #region Monobehaviour methods
#if UNITY_EDITOR
        private void OnValidate()
        {
            if (itemSlotsGrid != null)
            {
                itemSlots = itemSlotsGrid.GetComponentsInChildren<AbstractItemSlot>();
            }
            else
            {
                Debug.LogError("No [ItemSlotsGrid] at: "+this);
            }
            if (abstractItemTooltip == null)
            {
                Debug.LogError("No [AbstractItemTooltip] at: "+this);
            }
        }
#endif
        #endregion

        #region Methods

        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentInventory(_currentEntity.Inventory);
        }

        private void SetCurrentInventory(Inventory _inventory)
        {
            if (currentInventory != null)
            {
                for (int k = 0; k < itemSlots.Length; k++)
                {
                    itemSlots[k].RemoveOnShortPressListener(this);
                }
                currentInventory.OnInventoryChanged.Remove(OnInventoryChanged);
            }
            currentInventory = _inventory;
            for (int k = 0; k < itemSlots.Length; k++)
            {
                itemSlots[k].AddOnShortPressListener(this);
            }
            currentInventory.OnInventoryChanged.Add(OnInventoryChanged);
            RefreshInventoryPanel();
        }

        private void OnInventoryChanged(Entity _entity, EventArgs _eventArgs)
        {
            RefreshInventoryPanel();
        }

        public void OnAbstractItemSlotShortPress(AbstractItem _abstractItem)
        {
            abstractItemTooltip.ShowTooltip(_abstractItem, currentInventory.Entity, AbstractItemTooltipContext.FromInventory);
        }

        private void RefreshInventoryPanel()
        {
            int i = 0;
            for (; i < currentInventory.Items.Count && i < itemSlots.Length; i++)
            {
                itemSlots[i].Item = currentInventory.Items[i];
            }

            for (; i < itemSlots.Length; i++)
            {
                itemSlots[i].Item = null;
            }
        }

        public void ShowInventoryPanel()
        {
            gameObject.SetActive(true);
        }

        public void CloseInventoryPanel()
        {
            gameObject.SetActive(false);
        }

        #endregion



    }
}
