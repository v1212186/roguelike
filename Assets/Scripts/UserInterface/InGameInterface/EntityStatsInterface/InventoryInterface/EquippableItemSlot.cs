﻿using ItemsSystem;
using UnityEngine;

namespace UserInterface.InGameInterface.EntityStatsInterface.InventoryInterface
{
    public class EquippableItemSlot : AbstractItemSlot
    {
        #region Variables
        [SerializeField]
        private EquipmentSlot equipmentSlot;
        public EquipmentSlot EquipmentSlot { get { return equipmentSlot; } }
        #endregion

        #region Monobehaviour methods
#if UNITY_EDITOR
        private void OnValidate()
        {
            gameObject.name = equipmentSlot.ToString() + "Slot";
        }
#endif
        #endregion
       
    }
}
