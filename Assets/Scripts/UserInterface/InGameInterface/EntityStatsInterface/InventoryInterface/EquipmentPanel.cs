﻿using System;
using ActionsSystem;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.InventoryComponent;
using ItemsSystem;
using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface.AbstractItemTooltipInterface;

namespace UserInterface.InGameInterface.EntityStatsInterface.InventoryInterface
{
    public class EquipmentPanel : MonoBehaviour, IOnAbstractItemSlotShortPress
    {
        #region Variables
        [SerializeField]
        private EquippableItemSlot helmetSlot;
        [SerializeField]
        private EquippableItemSlot armorSlot;
        [SerializeField]
        private EquippableItemSlot weaponSlot;
        [SerializeField]
        private EquippableItemSlot glovesSlot;
        [SerializeField]
        private EquippableItemSlot bootsSlot;
        [SerializeField]
        private Inventory currentInventory;
        [SerializeField]
        private AbstractItemTooltip abstractItemTooltip;
        #endregion

        #region Monobehaviour methods
#if UNITY_EDITOR
        private void OnValidate()
        {
            EquippableItemSlot[] equippableItemSlots = GetComponentsInChildren<EquippableItemSlot>();
            for (int i = 0; i < equippableItemSlots.Length; i++)
            {
                switch (equippableItemSlots[i].EquipmentSlot)
                {
                    case EquipmentSlot.Helmet:
                        helmetSlot = equippableItemSlots[i];
                        break;
                    case EquipmentSlot.Armor:
                        armorSlot = equippableItemSlots[i];
                        break;
                    case EquipmentSlot.Weapon:
                        weaponSlot = equippableItemSlots[i];
                        break;
                    case EquipmentSlot.Gloves:
                        glovesSlot = equippableItemSlots[i];
                        break;
                    case EquipmentSlot.Boots:
                        bootsSlot = equippableItemSlots[i];
                        break;
                    default: break;
                }
            }
        }
#endif
        #endregion

        #region Methods

        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentInventory(_currentEntity.Inventory);
        }

        private void SetCurrentInventory(Inventory _inventory)
        {
            if (currentInventory != null)
            {
                helmetSlot.RemoveOnShortPressListener(this);
                armorSlot.RemoveOnShortPressListener(this);
                weaponSlot.RemoveOnShortPressListener(this);
                glovesSlot.RemoveOnShortPressListener(this);
                bootsSlot.RemoveOnShortPressListener(this);
                currentInventory.OnInventoryChanged.Remove(OnInventoryChanged);
            }
            currentInventory = _inventory;
            helmetSlot.AddOnShortPressListener(this);
            armorSlot.AddOnShortPressListener(this);
            weaponSlot.AddOnShortPressListener(this);
            glovesSlot.AddOnShortPressListener(this);
            bootsSlot.AddOnShortPressListener(this);
            currentInventory.OnInventoryChanged.Remove(OnInventoryChanged);
            RefreshEquipmentPanel();

        }

        private void OnInventoryChanged(Entity _entity, EventArgs _eventArgs)
        {
            RefreshEquipmentPanel();
        }

        //TODO передавать тут эквипабл, т.к. используется только он
        public void OnAbstractItemSlotShortPress(AbstractItem _abstractItem)
        {
            abstractItemTooltip.ShowTooltip(_abstractItem, currentInventory.Entity, AbstractItemTooltipContext.FromEquipment);
        }

        private void RefreshEquipmentPanel()
        {
            helmetSlot.Item = currentInventory.HelmetItem;
            armorSlot.Item = currentInventory.ArmorItem;
            weaponSlot.Item = currentInventory.WeaponItem;
            glovesSlot.Item = currentInventory.GlovesItem;
            bootsSlot.Item = currentInventory.BootsItem;
        }

        public void ShowEquipmentPanel()
        {
            gameObject.SetActive(true);
        }

        public void CloseEquipmentPanel()
        {
            gameObject.SetActive(false);
        }

        #endregion


    }
}
