﻿using System;
using ItemsSystem;
using UnityEngine;
using UnityEngine.UI;
using UserInterface.InGameInterface.InteractableInterfaceElement;

namespace UserInterface.InGameInterface.EntityStatsInterface.InventoryInterface
{
    public interface IOnAbstractItemSlotShortPress
    {
        void OnAbstractItemSlotShortPress(AbstractItem _abstractItem);
    }

    public interface IOnAbstractItemSlotLongPress
    {
        void OnAbstractItemSlotLongPress(AbstractItem _abstractItem);
    }

    public class AbstractItemSlot : InteractableElement
    {
        #region Variables
        protected AbstractItem item;
        public AbstractItem Item
        {
            get { return item; }
            set
            {
                item = value;
                if (item == null)
                {
                    itemImage.enabled = false;
                    itemImage.sprite = null;
                }
                else
                {
                    itemImage.enabled = true;
                    itemImage.sprite = item.ItemSprite;
                }
            }
        }

        [SerializeField]
        private Image itemImage;
        #region Events
        private event Action<AbstractItem> OnAbstractItemSlotShortPress;
        public void AddOnShortPressListener(IOnAbstractItemSlotShortPress _listener)
        {
            OnAbstractItemSlotShortPress += _listener.OnAbstractItemSlotShortPress;
        }
        public void RemoveOnShortPressListener(IOnAbstractItemSlotShortPress _listener)
        {
            OnAbstractItemSlotShortPress -= _listener.OnAbstractItemSlotShortPress;
        }
        public void FireOnAbstractItemSlotShortPressEvent(AbstractItem _item)
        {
            if (_item != null && OnAbstractItemSlotShortPress != null)
            {
                OnAbstractItemSlotShortPress(_item);
            }
        }

        private event Action<AbstractItem> OnAbstractItemSlotLongPress;
        public void AddOnLongPressListener(IOnAbstractItemSlotLongPress _listener)
        {
            OnAbstractItemSlotLongPress += _listener.OnAbstractItemSlotLongPress;
        }
        public void RemoveOnLongPressListener(IOnAbstractItemSlotLongPress _listener)
        {
            OnAbstractItemSlotLongPress -= _listener.OnAbstractItemSlotLongPress;
        }
        public void FireOnAbstractItemSlotLongPressEvent(AbstractItem _item)
        {
            if (_item != null && OnAbstractItemSlotLongPress != null)
            {
                OnAbstractItemSlotLongPress(_item);
            }
        }

        #endregion
        #endregion

        #region Methods

        public override void OnShortPress()
        {
            FireOnAbstractItemSlotShortPressEvent(item);
        }

        public override void OnLongPress()
        {
            FireOnAbstractItemSlotLongPressEvent(item);
        }

        #endregion

    }
}
