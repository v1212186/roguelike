﻿using EntitiesSystem;
using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface.EntityStatsInterface.AttributesInterface;
using UserInterface.InGameInterface.EntityStatsInterface.InventoryInterface;

namespace UserInterface.InGameInterface.EntityStatsInterface
{
    public class EntityStatsPanel : MonoBehaviour
    {

        #region Variables

        [SerializeField]
        private InventoryPanel inventoryPanel;
        [SerializeField]
        private EquipmentPanel equipmentPanel;
        [SerializeField]
        private AttributesPanel attributesPanel;
        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            inventoryPanel = GetComponentInChildren<InventoryPanel>();
            equipmentPanel = GetComponentInChildren<EquipmentPanel>();
            attributesPanel = GetComponentInChildren<AttributesPanel>();
        }
#endif

        #endregion

        #region Methods
        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            SetCurrentEntity(_previousEntity, _currentEntity);
        }

        private void SetCurrentEntity(Entity _previousEntity, Entity _currentEntity)
        {
            inventoryPanel.OnCurrentPlayerActorChanged(_previousEntity, _currentEntity);
            equipmentPanel.OnCurrentPlayerActorChanged(_previousEntity, _currentEntity);
            attributesPanel.OnCurrentPlayerActorChanged(_previousEntity, _currentEntity);
        }

        public void ShowEntityStatsPanel()
        {
            inventoryPanel.ShowInventoryPanel();
            equipmentPanel.ShowEquipmentPanel();
            attributesPanel.ShowAttributesPanel();
            gameObject.SetActive(true);
        }

        public void CloseEntityStatsPanel()
        {
            inventoryPanel.CloseInventoryPanel();
            equipmentPanel.CloseEquipmentPanel();
            attributesPanel.CloseAttributesPanel();
            gameObject.SetActive(false);
        }

        #endregion

    }
}
