﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace UserInterface.InGameInterface.InteractableInterfaceElement
{
    public enum PressType
    {
        Short, Long
    }

    public abstract class InteractableElement : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IUpdateSelectedHandler, IPointerExitHandler, IPointerEnterHandler
    {
        #region Variables
        [SerializeField]
        private float elementHeldTimer = 0.0f;
        [SerializeField]
        private const float HoldTime = 1.0f;
        [SerializeField]
        private bool isUpdating = false;
        [SerializeField]
        private bool isInside = false;
        #endregion

        #region Methods

        public void OnPointerDown(PointerEventData _eventData)
        {
            elementHeldTimer = 0.0f;
            isUpdating = true;
        }

        public void OnPointerUp(PointerEventData _eventData)
        {
            if (elementHeldTimer <= HoldTime)
            {
                isUpdating = false;
                if (isInside)
                {
                    OnShortPress();
                }
            }
        }

        public void OnUpdateSelected(BaseEventData _eventData)
        {
            if (!isUpdating)
            {
                return;
            }
            elementHeldTimer += Time.deltaTime;
            if (elementHeldTimer > HoldTime)
            {
                isUpdating = false;
                if (isInside)
                {
                    OnLongPress();
                }
            }
        }

        public void OnPointerExit(PointerEventData _eventData)
        {
            isInside = false;
        }

        public void OnPointerEnter(PointerEventData _eventData)
        {
            isInside = true;
        }

        public abstract void OnShortPress();

        public abstract void OnLongPress();

        #endregion

    }
}
