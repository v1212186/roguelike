﻿using System.Collections.Generic;
using ActionsSystem.Items;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.ItemComponent;
using LevelsSystem;
using UnityEngine;
using UserInterface.InGameInterface.AbstractItemTooltipInterface;
using Utilities;

namespace UserInterface.InGameInterface.ItemPickupInterface
{
    public class ItemPickupPanel : MonoBehaviour
    {

        #region Variables

        private Level currentLevel;

        [SerializeField]
        private ItemPickupIndicator itemPickupIndicatorPrefab;
        [SerializeField]
        private List<ItemPickupIndicator> itemPickupIndicators = new List<ItemPickupIndicator>();
        [SerializeField]
        private Transform indicatorsGrid;
        [SerializeField]
        private AbstractItemTooltip abstractItemTooltip;
        [SerializeField]
        private Entity currentEntity;
        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (itemPickupIndicatorPrefab == null)
            {
                Debug.LogError("No [ItemPickupIndicatorPrefab] reference at: " + this);
            }
            if (abstractItemTooltip == null)
            {
                Debug.LogError("No [AbstractItemTooltip] reference at: " + this);
            }
            indicatorsGrid = transform;
        }
#endif

        #endregion

        #region Methods

        public void OnLevelChanged(Level _previousLevel, Level _currentLevel)
        {
            currentLevel = _currentLevel;
        }

        public void OnCurrentPlayerActorChanged(Entity _previousEntity, Entity _currentEntity)
        {
            if (_previousEntity != null)
            {
                _previousEntity.OnEntityPositionChanged.Remove(OnEntityPositionChanged);
            }
            _currentEntity.OnEntityPositionChanged.Add(OnEntityPositionChanged);
            currentEntity = _currentEntity;
            RefreshPanel(currentLevel.GetCellAtPosition(currentEntity.Position).HasEntitiesWithEntityComponent<Item>());
        }

        private void OnEntityPositionChanged(Entity _entity, ValueChangeEventArgs<Vector2Int> _valueChangeEventArgs)
        {
            RefreshPanel(currentLevel.GetCellAtPosition(_entity.Position).HasEntitiesWithEntityComponent<Item>());
        }

        public void RefreshPanel(Item[] _items)
        {
            if (_items.Length == 0)
            {
                ClosePanel();
                return;
            }

            for (int i = 0; i < itemPickupIndicators.Count; i++)
            {
                itemPickupIndicators[i].OnShortPressEvent.Remove(OnShortPressEvent);
                Destroy(itemPickupIndicators[i].gameObject);
            }
            itemPickupIndicators.Clear();
            for (int i = 0; i < _items.Length; i++)
            {
                ItemPickupIndicator pickupIndicator = Instantiate(itemPickupIndicatorPrefab.gameObject, indicatorsGrid)
                    .GetComponent<ItemPickupIndicator>();
                pickupIndicator.Item = _items[i];
                pickupIndicator.OnShortPressEvent.Add(OnShortPressEvent);
                itemPickupIndicators.Add(pickupIndicator);
            }
            ShowPanel();
        }

        private void OnShortPressEvent(object _o, ItemInteractableEventArgs _itemInteractableEventArgs)
        {
            OnAbstractItemSlotShortPress(_itemInteractableEventArgs.Item);
        }

        public void ShowPanel()
        {
            gameObject.SetActive(true);
        }

        public void ClosePanel()
        {
            gameObject.SetActive(false);
        }

        //TODO доделать
        public void OnAbstractItemSlotShortPress(Item _item)
        {
            abstractItemTooltip.ShowTooltip(_item.AbstractItem, currentEntity, AbstractItemTooltipContext.None, new PickUpItemAction(currentEntity, _item));
        }
        #endregion
    }
}
