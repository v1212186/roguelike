﻿using System;
using EntitiesSystem.EntityComponents.ItemComponent;
using UnityEngine;
using UnityEngine.UI;
using UserInterface.InGameInterface.InteractableInterfaceElement;
using Utilities;

namespace UserInterface.InGameInterface.ItemPickupInterface
{
    public class ItemInteractableEventArgs : EventArgs
    {
        #region Varibles

        public readonly Item Item;

        #endregion

        #region Constructor

        public ItemInteractableEventArgs(Item _item)
        {
            Item = _item;
        }

        #endregion
    }

    public class ItemPickupIndicator : InteractableElement
    {

        #region Variables

        protected Item item;
        public Item Item
        {
            get { return item; }
            set
            {
                item = value;
                if (item == null)
                {
                    itemImage.enabled = false;
                    itemImage.sprite = null;
                }
                else
                {
                    itemImage.enabled = true;
                    itemImage.sprite = item.AbstractItem.ItemSprite;
                }
            }
        }

        [SerializeField]
        private Image itemImage;

        public GameEvent<object, ItemInteractableEventArgs> OnShortPressEvent = new GameEvent<object, ItemInteractableEventArgs>();

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR
        private void OnValidate()
        {
            itemImage = GetComponent<Image>();
        }
#endif

        #endregion

        #region Methods

        //public void 

        #endregion

        public override void OnShortPress()
        {
            OnShortPressEvent.Raise(this, new ItemInteractableEventArgs(Item));
        }

        public override void OnLongPress()
        {
            OnShortPressEvent.Raise(this, new ItemInteractableEventArgs(Item));
        }

    }
}
