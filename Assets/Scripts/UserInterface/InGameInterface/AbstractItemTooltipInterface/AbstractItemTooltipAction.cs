﻿using System;
using ActionsSystem;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.ActorComponent;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.InGameInterface.AbstractItemTooltipInterface
{
    public class AbstractItemTooltipAction : MonoBehaviour
    {

        #region Variables

        [SerializeField]
        private Button actionButton;
        [SerializeField]
        private Text actionDescription;
        [SerializeField]
        private Entity actorEntity;
        private AbstractAction abstractAction;
        private Action closeTooltip;

        #endregion

        #region Monobehaviour methods

#if UNITY_EDITOR

        private void OnValidate()
        {
            actionDescription = GetComponentInChildren<Text>();
            actionButton = GetComponent<Button>();
        }

#endif

        private void Start()
        {
            actionButton.onClick.AddListener(PerformAction);
        }

        private void PerformAction()
        {
            PlayerActor actor = actorEntity.GetComponent<PlayerActor>();
            if (actor.SetAbstractActionNew(abstractAction))
            {
                closeTooltip();
            }
        }

        #endregion

        #region Methods

        public void SetUpTooltipAction(Entity _entity, AbstractAction _abstractAction, Action _closeTooltip)
        {
            actorEntity = _entity;
            abstractAction = _abstractAction;
            actionDescription.text = abstractAction.GetDescription();
            closeTooltip = _closeTooltip;
        }

        #endregion

    }
}
