﻿using System.Collections.Generic;
using ActionsSystem;
using ActionsSystem.Items;
using EntitiesSystem;
using ItemsSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.InGameInterface.AbstractItemTooltipInterface
{
    public enum AbstractItemTooltipContext
    {
        FromInventory, FromEquipment, None
    }

    public class AbstractItemTooltip : MonoBehaviour
    {
        #region Variables
        [SerializeField]
        private Image imageSprite;
        [SerializeField]
        private Text itemName;
        [SerializeField]
        private Text itemDescription;
        [SerializeField]
        private AbstractItem abstractItem;
        public AbstractItem AbstractItem
        {
            get { return abstractItem; }
            set
            {
                abstractItem = value;
                imageSprite.sprite = abstractItem.ItemSprite;
                itemName.text = abstractItem.ItemName;
                itemDescription.text = abstractItem.GetDescription();
            }
        }
        [SerializeField]
        private Transform actionsGrid;
        [SerializeField]
        private AbstractItemTooltipAction itemTooltipAction;
        [SerializeField]
        private List<AbstractItemTooltipAction> itemTooltipActions;
        [SerializeField]
        private Entity entity;
        #endregion

        #region Methods

        public void ShowTooltip(AbstractItem _abstractItem, Entity _entity, AbstractItemTooltipContext _tooltipContext, params AbstractAction[] _customActions)
        {
            AbstractItem = _abstractItem;
            entity = _entity;
            CreateAbstractItemTooltipActions(abstractItem, _tooltipContext, _customActions);
            gameObject.SetActive(true);
        }

        public void CreateAbstractItemTooltipActions(AbstractItem _abstractItem, AbstractItemTooltipContext _tooltipContext, params AbstractAction[] _customActions)
        {
            if (itemTooltipActions == null)
            {
                itemTooltipActions = new List<AbstractItemTooltipAction>();
            }
            foreach (AbstractItemTooltipAction tooltipAction in itemTooltipActions)
            {
                Destroy(tooltipAction.gameObject);
            }
            itemTooltipActions.Clear();

            switch (_tooltipContext)
            {
                case AbstractItemTooltipContext.FromInventory:
                    if (abstractItem is EquippableItem)
                    {
                        itemTooltipActions.Add(CreateTooltipEquipItemAction());
                    }

                    if (abstractItem is UsableItem)
                    {
                        itemTooltipActions.Add(CreateTooltipUseItemAction());
                    }
                    itemTooltipActions.Add(CreateTooltipDropItemAction());
                    itemTooltipActions.Add(CreateTooltipDestroyItemAction());
                    break;
                case AbstractItemTooltipContext.FromEquipment:
                    if (abstractItem is EquippableItem)
                    {
                        itemTooltipActions.Add(CreateTooltipUnequipItemAction());
                    }

                    if (abstractItem is UsableItem)
                    {
                        itemTooltipActions.Add(CreateTooltipUseItemAction());
                    }
                    itemTooltipActions.Add(CreateTooltipDropItemAction());
                    itemTooltipActions.Add(CreateTooltipDestroyItemAction());
                    break;
                case AbstractItemTooltipContext.None:
                    break;
            }


            for (int i = 0; i < _customActions.Length; i++)
            {
                itemTooltipActions.Add(CreateTooltipFromAbstractAction(_customActions[i]));
            }
        }

        private AbstractItemTooltipAction CreateTooltipFromAbstractAction(AbstractAction _abstractAction)
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, _abstractAction, CloseTooltip);
            return action;
        }

        //TODO может вынести все экшоны в те классы что вызывают тултип
        private AbstractItemTooltipAction CreateTooltipDestroyItemAction()
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, new DestroyItemAction(entity, entity, abstractItem), CloseTooltip);
            return action;
        }

        private AbstractItemTooltipAction CreateTooltipDropItemAction()
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, new DropItemAcion(entity, abstractItem), CloseTooltip);
            return action;
        }

        private AbstractItemTooltipAction CreateTooltipUseItemAction()
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, new UseItemAction(entity, entity, abstractItem as UsableItem), CloseTooltip);
            return action;
        }

        private AbstractItemTooltipAction CreateTooltipUnequipItemAction()
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, new UnequipItemAction(entity, entity, abstractItem as EquippableItem), CloseTooltip);
            return action;
        }

        private AbstractItemTooltipAction CreateTooltipEquipItemAction()
        {
            AbstractItemTooltipAction action =
                Instantiate(itemTooltipAction.gameObject, actionsGrid).GetComponent<AbstractItemTooltipAction>();
            action.SetUpTooltipAction(entity, new EquipItemAction(entity, entity, abstractItem as EquippableItem), CloseTooltip);
            return action;
        }

        public void CloseTooltip()
        {
            gameObject.SetActive(false);
        }

        #endregion

    }
}
