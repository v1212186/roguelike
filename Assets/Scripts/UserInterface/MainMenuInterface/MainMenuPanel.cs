﻿using UnityEngine;
using UnityEngine.UI;

namespace UserInterface.MainMenuInterface
{
    public class MainMenuPanel : MonoBehaviour
    {

        #region Variables

        [SerializeField]
        private Button startGameButton;
        [SerializeField]
        private Button exitGameButton;
        [SerializeField]
        private Game game;

        #endregion

        #region Monobehaviour methods

        private void Start()
        {
            startGameButton.onClick.AddListener(StartGame);
            exitGameButton.onClick.AddListener(ExitGame);
        }

#if UNITY_EDITOR

        private void OnValidate()
        {
            game = FindObjectOfType<Game>();
        }

#endif

        #endregion

        #region Methods

        public void ShowMainMenuPanel()
        {
            gameObject.SetActive(true);
        }

        public void CloseMainMenuPanel()
        {
            gameObject.SetActive(false);
        }

        private void StartGame()
        {
            game.StartGame();
        }

        private void ExitGame()
        {
            Application.Quit();
        }
        #endregion

    }
}
