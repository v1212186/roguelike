﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputHandler {

    public Vector2Int GetDirection()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            return Vector2Int.left;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            return Vector2Int.right;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            return Vector2Int.up;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            return Vector2Int.down;
        }
        return Vector2Int.zero;
    }


    //public Vector2Int GetPosition()
    //{
    //    Event @event = Event.KeyboardEvent(string.Empty);
    //    @event.keyCode = key;
    //    InputManager.m_inputEvents.AddLast(@event);
    //}

}
