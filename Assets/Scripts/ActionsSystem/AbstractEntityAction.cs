﻿using System.Collections;
using EntitiesSystem;
using UnityEngine;
using Utilities;

namespace ActionsSystem
{
    public abstract class AbstractEntityAction : AbstractAction
    {

        #region Variables

        protected Entity targetEntity;

        #endregion

        #region Constructors

        protected AbstractEntityAction(Entity _actorEntity, NotNull<Entity> _targetEntity, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
            targetEntity = _targetEntity;
        }

        protected AbstractEntityAction(Entity _actorEntity, NotNull<Entity> _targetEntity) : base(_actorEntity)
        {
            targetEntity = _targetEntity;
        }

        protected AbstractEntityAction(Entity _actorEntity, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
            targetEntity = null;
        }

        protected AbstractEntityAction(Entity _actorEntity) : base(_actorEntity)
        {
            targetEntity = null;
        }

        #endregion

        #region Methods

        public override void OnProcess()
        {
            //Выделить в общую часть
            if (recentActionResult.IsPerforming || recentActionResult.IsDone || recentActionResult.IsFailed)
            {
                return;
            }
            if (targetEntity == null)
            {
                //TODO Таргет селекшин либо ретурнить фейлед экшон либо еще что, скорее всего вынести в отдельный абстрактный метод для оверрайда
                OnTargetEntityNull();
                return;
            }
            if (!CanApplyToEntity(targetEntity))
            {
                //TODO Сделать таргет энетити нулл либо фейлед экшон(если не хотим продолжать), резалт не возвращать, потом уже либо таргет селекшн
                OnCantApplyToEntity();
                return;
            }
            //Выделить в общую часть

            if (targetEntity != null)
            {
                OnCanPerformAction();
                return;
            }

            //Выделить в общую часть
            //recentActionResult = new ActionResult(ActionResultFlags.Failed );
            //Выделить в общую часть
        }

        protected abstract void OnTargetEntityNull();

        protected abstract void OnCantApplyToEntity();

        protected abstract void OnCanPerformAction();

        public abstract bool CanApplyToEntity(NotNull<Entity> _targetEntity);

        #endregion
    }
}
