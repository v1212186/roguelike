using System;
using System.Collections;
using EntitiesSystem;
using ItemsSystem;
using UnityEngine;
using Utilities;

namespace ActionsSystem.Items
{
    public class UseItemAction : AbstractEntityAction
    {
        #region Variables

        private readonly UsableItem usableItem;

        #endregion

        #region Constructors

        public UseItemAction(Entity _actorEntity, NotNull<Entity> _targetEntity, NotNull<UsableItem> _usableItem, bool _endsTurn)
            : base(_actorEntity, _endsTurn)
        {
            targetEntity = _targetEntity;
            usableItem = _usableItem;
        }

        public UseItemAction(Entity _actorEntity, NotNull<Entity> _targetEntity, NotNull<UsableItem> _usableItem)
            : base(_actorEntity)
        {
            targetEntity = _targetEntity;
            usableItem = _usableItem;
        }

        #endregion

        #region Methods

        protected override void OnTargetEntityNull()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            recentActionResult = new ApplyStatusEffectAction(ActorEntity, targetEntity, usableItem.StatusEffect, usableItem);
        }

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.StatusEffectsSet != null;
        }

        public override string GetDescription()
        {
            return "Use item";
        }

        #endregion

    }
}
