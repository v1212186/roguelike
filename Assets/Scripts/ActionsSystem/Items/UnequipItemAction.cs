﻿using System;
using System.Collections;
using EntitiesSystem;
using ItemsSystem;
using Utilities;

namespace ActionsSystem.Items
{
    public class UnequipItemAction : AbstractEntityAction
    {

        #region Variables

        private readonly EquippableItem itemToUnequip;

        #endregion

        #region Constructor

        public UnequipItemAction(Entity _actorEntity, NotNull<Entity> _targetEntity, EquippableItem _itemToUnequip) : base(_actorEntity, _targetEntity)
        {
            itemToUnequip = _itemToUnequip;
        }

        #endregion

        #region Methods

        protected override void OnTargetEntityNull()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            if (targetEntity.Inventory.UnequipItem(itemToUnequip))
            {
                endsTurn = true;
                recentActionResult = new ActionResult(ActionResultFlags.Done);
            }
            else
            {
                endsTurn = false;
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
            }
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.Inventory != null;
        }

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        public override string GetDescription()
        {
            return "Unequip item";
        }

        #endregion


    }
}
