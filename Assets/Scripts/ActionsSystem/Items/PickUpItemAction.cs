﻿using System.Collections;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.ItemComponent;
using Utilities;

namespace ActionsSystem.Items
{
    public class PickUpItemAction : AbstractEntityAction
    {

        #region Variables

        private readonly Item item;

        #endregion

        #region Constructors

        public PickUpItemAction(Entity _actorEntity, NotNull<Item> _targetEntity, bool _endsTurn) : base(_actorEntity, _targetEntity.Value.Entity, _endsTurn)
        {
            item = _targetEntity;
            targetEntity = _actorEntity;
        }

        public PickUpItemAction(Entity _actorEntity, NotNull<Item> _targetEntity) : this(_actorEntity, _targetEntity.Value, false)
        {
            item = _targetEntity;
            targetEntity = _actorEntity;
        }

        #endregion

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        protected override void OnTargetEntityNull()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            if (ActorEntity.Inventory.AddItem(item.AbstractItem))
            {
                item.Entity.DestroyEntity(); //TODO делать это через какой-нибудь менеджер
                endsTurn = true;
                recentActionResult = new ActionResult(ActionResultFlags.Done);
            }
            else
            {
                endsTurn = false;
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
            }
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.Inventory != null;
        }

        public override string GetDescription()
        {
            return "Pickup item";
        }
    }
}
