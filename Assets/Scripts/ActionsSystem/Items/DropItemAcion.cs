﻿using System.Collections;
using EntitiesSystem;
using ItemsSystem;

namespace ActionsSystem.Items
{
    public class DropItemAcion : AbstractAction
    {
        #region Variables

        private readonly AbstractItem abstractItem;

        #endregion

        #region Constructors

        public DropItemAcion(Entity _actorEntity, AbstractItem _abstractItem, bool _endsTurn)
            : base(_actorEntity, _endsTurn)
        {
            abstractItem = _abstractItem;
        }

        public DropItemAcion(Entity _actorEntity, AbstractItem _abstractItem)
            : base(_actorEntity)
        {
            abstractItem = _abstractItem;
        }

        #endregion

        #region Methods

        public override void OnProcess()
        {
            if (recentActionResult.IsPerforming || recentActionResult.IsDone || recentActionResult.IsFailed)
            {
                return;
            }
            if (!CanApplyToEntity(ActorEntity))
            {
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
                return;
            }

            if (ActorEntity.Inventory.RemoveItem(abstractItem))
            {
                Entity item = ActorEntity.Level.Game.EntityGenerator.GenerateItemEntityForAbstractItem(abstractItem);
                item.PlaceEntityOnLevel(ActorEntity.Level, ActorEntity.Position);
                recentActionResult = new ActionResult(ActionResultFlags.Done);
                return;
            }

            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        public override string GetDescription()
        {
            return "Drop item";
        }

        public bool CanApplyToEntity(Entity _targetEntity)
        {
            return _targetEntity.Inventory != null;
        }

        #endregion

    }
}
