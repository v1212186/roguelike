﻿using System.Collections;
using EntitiesSystem;
using ItemsSystem;
using Utilities;

namespace ActionsSystem.Items
{
    //TODO сделать общий класс для экшенов с айтемами
    //TODO исправить, но перед этим возможно сделать экшоны в интерактабл 
    public class DestroyItemAction : AbstractEntityAction
    {

        #region Variables

        private readonly AbstractItem abstractItem;

        #endregion

        #region Constructor

        public DestroyItemAction(Entity _actorEntity, Entity _targetEntity, NotNull<AbstractItem> _abstractItem, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
            targetEntity = _targetEntity;
            abstractItem = _abstractItem;
        }

        public DestroyItemAction(Entity _actorEntity, Entity _targetEntity, NotNull<AbstractItem> _abstractItem) : base(_actorEntity)
        {
            targetEntity = _targetEntity;
            abstractItem = _abstractItem;
        }

        #endregion

        #region Methods

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        protected override void OnTargetEntityNull()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            if (targetEntity.Inventory.RemoveItem(abstractItem))
            {
                endsTurn = true;
                recentActionResult = new ActionResult(ActionResultFlags.Done);
            }
            else
            {
                endsTurn = false;
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
            }
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.Inventory != null;
        }

        public override string GetDescription()
        {
            return "Destroy item";
        }

        #endregion


    }
}
