﻿using System;
using System.Collections;
using EntitiesSystem;
using ItemsSystem;
using Utilities;

namespace ActionsSystem.Items
{
    public class EquipItemAction : AbstractEntityAction
    {

        #region Variables

        private readonly EquippableItem equippableItem;

        #endregion

        #region Constructor

        public EquipItemAction(Entity _actorEntity, NotNull<Entity> _targetEntity, NotNull<EquippableItem> _equippbleItem, bool _endsTurn) : base(_actorEntity, _targetEntity, _endsTurn)
        {
            equippableItem = _equippbleItem;
        }

        public EquipItemAction(Entity _actorEntity, NotNull<Entity> _targetEntity, NotNull<EquippableItem> _equippbleItem) : base(_actorEntity, _targetEntity)
        {
            equippableItem = _equippbleItem;
        }

        #endregion

        protected override void OnTargetEntityNull()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            EquippableItem item;
            if (targetEntity.Inventory.EquipItem(equippableItem, out item))
            {
                endsTurn = true;
                recentActionResult = new ActionResult(ActionResultFlags.Done);
            }
            else
            {
                endsTurn = false;
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
            }
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.Inventory != null;
        }

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        public override string GetDescription()
        {
            return "Equip item";
        }

    }
}
