﻿using System.Collections;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using EntitiesSystem.EntityComponents.InteractableComponent;
using Utilities;

namespace ActionsSystem
{
    public class InteractAction : AbstractEntityAction
    {

        #region Constructors

        public InteractAction(Entity _actorEntity, NotNull<Entity> _targetEntity, bool _endsTurn) : base(_actorEntity, _targetEntity, _endsTurn)
        {
        }

        public InteractAction(Entity _actorEntity, NotNull<Entity> _targetEntity) : base(_actorEntity, _targetEntity)
        {
        }

        #endregion

        #region Methods

        protected override IEnumerator PerformAction()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Performing);

            ActorEntity.CustomAnimator.MirrorSprite(targetEntity.Position - ActorEntity.Position);
            yield return ActorEntity.StartCoroutine(ActorEntity.CustomAnimator.PlayAnimation(AnimationName.Attack));
            endsTurn = targetEntity.Interactable.Interact(ActorEntity);

            recentActionResult = new ActionResult(ActionResultFlags.Done);
        }

        public override string GetDescription()
        {
            return "Interact with target entity";
        }

        protected override void OnTargetEntityNull()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            endsTurn = false;
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            ActorEntity.StartCoroutine(PerformAction());
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.HasEntityComponent<Interactable>();
        }

        #endregion
    }
}
