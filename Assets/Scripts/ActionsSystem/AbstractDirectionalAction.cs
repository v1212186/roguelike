﻿using System.Collections;
using EntitiesSystem;
using UnityEngine;
using Utilities;

namespace ActionsSystem
{
    public abstract class AbstractDirectionalAction : AbstractAction {

        #region Variables

        protected Vector2Int direction;

        #endregion

        #region Constructor
        protected AbstractDirectionalAction(NotNull<Entity> _actorEntity, Vector2Int _direction, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
            direction = _direction;
        }

        protected AbstractDirectionalAction(NotNull<Entity> _actorEntity, Vector2Int _direction) : base(_actorEntity)
        {
            direction = _direction;
        }

        protected AbstractDirectionalAction(NotNull<Entity> _actorEntity, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
            direction = Vector2Int.zero;
        }

        protected AbstractDirectionalAction(NotNull<Entity> _actorEntity) : base(_actorEntity)
        {
            direction = Vector2Int.zero;
        }
        #endregion

        #region Methods
        public override void OnProcess()
        {
            //Выделить в общую часть
            if (recentActionResult.IsPerforming || recentActionResult.IsDone || recentActionResult.IsFailed)
            {
                return;
            }
            if (direction == Vector2Int.zero)
            {
                //Таргет селекшин либо ретурнить фейлед экшон либо еще что, скорее всего вынести в отдельный абстрактный метод для оверрайда
                OnNoDirection();
                return;
            }
            if (!CanApplyInDirection(direction))
            {
                //Сделать таргет энетити нулл либо фейлед экшон(если не хотим продолжать), резалт не возвращать, потом уже либо таргет селекшн
                OnCantApplyInDirection();
                return;
            }
            //Выделить в общую часть

            if (direction != Vector2Int.zero)
            {
                OnCanPerformAction();
                return;
            }

            //Выделить в общую часть
            //recentActionResult = new ActionResult(ActionResultFlags.Failed );
            //Выделить в общую часть
        }

        protected abstract void OnNoDirection();

        protected abstract void OnCantApplyInDirection();

        protected abstract void OnCanPerformAction();

        public abstract bool CanApplyInDirection(Vector2Int _direction); 
        #endregion
    }
}
