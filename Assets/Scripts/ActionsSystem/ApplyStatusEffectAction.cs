﻿using System.Collections;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using ItemsSystem;
using StatusEffectsSystem;
using Utilities;

namespace ActionsSystem
{
    public class ApplyStatusEffectAction : AbstractEntityAction
    {

        #region Variables

        private readonly AbstractStatusEffect statusEffect;
        private readonly UsableItem usableItem;

        #endregion

        #region Constructors
        public ApplyStatusEffectAction(Entity _actorEntity, NotNull<Entity> _targetEntity, AbstractStatusEffect _statusEffect, bool _endsTurn)
        : base(_actorEntity, _targetEntity, _endsTurn)
        {
            statusEffect = _statusEffect;
            usableItem = null;
        }

        public ApplyStatusEffectAction(Entity _actorEntity, NotNull<Entity> _targetEntity, AbstractStatusEffect _statusEffect)
            : base(_actorEntity, _targetEntity)
        {
            statusEffect = _statusEffect;
            usableItem = null;
        }

        public ApplyStatusEffectAction(Entity _actorEntity, NotNull<Entity> _targetEntity, AbstractStatusEffect _statusEffect, UsableItem _usableItem, bool _endsTurn)
            : base(_actorEntity, _targetEntity, _endsTurn)
        {
            statusEffect = _statusEffect;
            usableItem = _usableItem;
        }

        public ApplyStatusEffectAction(Entity _actorEntity, NotNull<Entity> _targetEntity, AbstractStatusEffect _statusEffect, UsableItem _usableItem)
            : base(_actorEntity, _targetEntity)
        {
            statusEffect = _statusEffect;
            usableItem = _usableItem;
        }
        #endregion

        protected override IEnumerator PerformAction()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Performing);

            ActorEntity.CustomAnimator.MirrorSprite(targetEntity.Position - ActorEntity.Position);
            yield return ActorEntity.StartCoroutine(ActorEntity.CustomAnimator.PlayAnimation(AnimationName.Attack));

            if (targetEntity.StatusEffectsSet.ApplyStatusEffect(statusEffect, targetEntity))
            {
                if (usableItem != null)
                {
                    ActorEntity.Inventory.RemoveItem(usableItem); 
                }
                recentActionResult = new ActionResult(ActionResultFlags.Done);
            }
            else
            {
                recentActionResult = new ActionResult(ActionResultFlags.Failed);
            }
        }

        protected override void OnTargetEntityNull()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            ActorEntity.StartCoroutine(PerformAction());
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.CharacterAttributes != null;
        }

        public override string GetDescription()
        {
            return "Applies " + statusEffect.GetDescription() + " effect";
        }
    }
}
