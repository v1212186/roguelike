﻿using System;

namespace ActionsSystem
{

    [Flags]
    public enum ActionResultFlags
    {
        Default = 0x0,
        Performing = 0x1,
        Done = 0x2,
        AwaitingInput = 0x4,
        Failed = 0x8,
        //Performing = 0x10,
    }

    public class ActionResult
    {
        #region Variables

        private ActionResultFlags flags;
        public ActionResultFlags Flags { get { return flags; } }

        private readonly AbstractAction alternateAction;
        public AbstractAction AlternateAction { get { return alternateAction; } }

        public bool IsFailed { get { return IsFlagSet(ActionResultFlags.Failed); } }
        public bool IsAwaitingInput { get { return IsFlagSet(ActionResultFlags.AwaitingInput); } }
        public bool IsDone { get { return IsFlagSet(ActionResultFlags.Done); } }
        public bool IsPerforming { get { return IsFlagSet(ActionResultFlags.Performing); } }

        #endregion

        #region Constructor

        public ActionResult(ActionResultFlags _flags)
        {
            flags = _flags;
        }

        public ActionResult(AbstractAction _alternateActionnate)
        {
            alternateAction = _alternateActionnate;
            flags = ActionResultFlags.Failed;
        }

        #endregion

        #region Methods

        private bool IsFlagSet(ActionResultFlags _flags)
        {
            return (flags & _flags) == _flags;
        }

        public void AddFlag(ActionResultFlags _flag)
        {
            flags = flags | _flag;
        }

        #endregion

    }
}
