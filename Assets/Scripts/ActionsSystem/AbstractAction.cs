﻿using System;
using System.Collections;
using EntitiesSystem;
using Utilities;

namespace ActionsSystem
{
    public abstract class AbstractAction
    {
        #region Varibles

        public readonly Entity ActorEntity;
        protected bool endsTurn;
        public bool EndsTurn { get { return endsTurn; } }
        protected ActionResult recentActionResult;
        public ActionResult RecentActionResult { get { return recentActionResult; } }
        #endregion

        #region Constructors

        protected AbstractAction(NotNull<Entity> _actorEntity, bool _endsTurn)
        {
            ActorEntity = _actorEntity;
            endsTurn = _endsTurn;
            recentActionResult = new ActionResult(ActionResultFlags.Default);

        }

        protected AbstractAction(NotNull<Entity> _actorEntity) : this(_actorEntity, true)
        {

        }

        #endregion

        #region Methods

        public static implicit operator ActionResult(AbstractAction _action)
        {
            return new ActionResult(_action);
        }

        public ActionResult ProcessAction()
        {
            OnProcess();
            return recentActionResult;
        }

        public abstract void OnProcess();

        protected abstract IEnumerator PerformAction();

        public abstract string GetDescription();

        #endregion

    }
}
