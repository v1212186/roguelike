﻿using System.Collections;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.CharacterAttributesComponent;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using EntitiesSystem.EntityComponents.InteractableComponent;
using UnityEngine;
using Utilities;

namespace ActionsSystem
{
    public class MoveAction : AbstractDirectionalAction
    {
        #region Constructors

        public MoveAction(NotNull<Entity> _actorEntity, Vector2Int _direction, bool _endsTurn) : base(_actorEntity, _direction, _endsTurn)
        {
        }

        public MoveAction(NotNull<Entity> _actorEntity, Vector2Int _direction) : base(_actorEntity, _direction)
        {
        }

        public MoveAction(NotNull<Entity> _actorEntity, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
        }

        public MoveAction(NotNull<Entity> _actorEntity) : base(_actorEntity)
        {
        }

        #endregion

        #region Methods

        protected override IEnumerator PerformAction()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Performing);

            ActorEntity.CustomAnimator.MirrorSprite(direction);
            ActorEntity.StartCoroutine(ActorEntity.CustomAnimator.PlayAnimationAndMoveActor(AnimationName.Walk, ActorEntity.Position + direction));
            yield return new WaitForEndOfFrame();

            recentActionResult = new ActionResult(ActionResultFlags.Done);
        }

        protected override void OnNoDirection()
        {
            InputHandler inputHandler = new InputHandler();
            direction = inputHandler.GetDirection();
            recentActionResult = new ActionResult(ActionResultFlags.AwaitingInput);
        }

        protected override void OnCantApplyInDirection()
        {
            Entity entity = ActorEntity.Level.GetEntityAtPositionWithComponent<CharacterAttributes>(ActorEntity.Position + direction);
            if (entity != null)
            {
                recentActionResult = new AttackAction(ActorEntity, entity);
                return;
            }

            entity = ActorEntity.Level.GetEntityAtPositionWithComponent<Interactable>(ActorEntity.Position + direction);
            if (entity != null)
            {
                recentActionResult = new InteractAction(ActorEntity, entity);
                return;
            }

            direction = Vector2Int.zero;
            recentActionResult = new ActionResult(ActionResultFlags.AwaitingInput);
        }

        protected override void OnCanPerformAction()
        {
            ActorEntity.StartCoroutine(PerformAction());
        }

        public override bool CanApplyInDirection(Vector2Int _direction)
        {
            return ActorEntity.Level.GetCellAtPosition(ActorEntity.Position + _direction) != null &&
                   !ActorEntity.Level.GetCellAtPosition(ActorEntity.Position + _direction).IsCollidableWithEntites();
        }

        public override string GetDescription()
        {
            return "Move in direction";
        }

        #endregion
    }
}
