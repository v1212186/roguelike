﻿using System;
using System.Collections;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.CustomAnimatorComponent;
using Utilities;

namespace ActionsSystem
{
    public class AttackAction : AbstractEntityAction
    {

        #region Varibles

        #endregion

        #region Constructors

        public AttackAction(Entity _actorEntity, Entity _targetEntity, bool _endsTurn) : base(_actorEntity, _targetEntity, _endsTurn)
        {
        }

        public AttackAction(Entity _actorEntity, Entity _targetEntity) : base(_actorEntity, _targetEntity, true)
        {
        }

        #endregion

        #region Methods

        protected override void OnTargetEntityNull()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCantApplyToEntity()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Failed);
        }

        protected override void OnCanPerformAction()
        {
            ActorEntity.StartCoroutine(PerformAction());
        }

        protected override IEnumerator PerformAction()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Performing);

            ActorEntity.CustomAnimator.MirrorSprite(targetEntity.Position - ActorEntity.Position);
            yield return ActorEntity.StartCoroutine(ActorEntity.CustomAnimator.PlayAnimation(AnimationName.Attack));
            targetEntity.CharacterAttributes.HealthAttribute.ChangeCurrentValue(-ActorEntity.CharacterAttributes.AttackAttribute.FinalValue);

            recentActionResult = new ActionResult(ActionResultFlags.Done);
        }

        public override bool CanApplyToEntity(NotNull<Entity> _targetEntity)
        {
            return _targetEntity.Value.CharacterAttributes != null;
        }

        public override string GetDescription()
        {
            return "Attack";
        }

        #endregion


    }
}
