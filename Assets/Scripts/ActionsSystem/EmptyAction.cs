﻿using System;
using System.Collections;
using EntitiesSystem;

namespace ActionsSystem
{
    public class EmptyAction : AbstractAction
    {

        #region Constructor

        public EmptyAction(Entity _actorEntity, bool _endsTurn) : base(_actorEntity, _endsTurn)
        {
        }

        #endregion

        #region Methods

        protected override IEnumerator PerformAction()
        {
            yield break;
        }

        public override string GetDescription()
        {
            return "Skip turn";
        }

        public override void OnProcess()
        {
            recentActionResult = new ActionResult(ActionResultFlags.Done);
        }

        #endregion
    }
}
