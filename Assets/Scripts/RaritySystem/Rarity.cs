﻿namespace RaritySystem
{
    public enum Rarity
    {
        Common, Rare, Epic, Legendary
    }
}
