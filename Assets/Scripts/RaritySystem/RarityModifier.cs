﻿namespace RaritySystem
{
    public static class RarityModifier
    {
        #region Methods
        public static float GetModifierForRarity(Rarity _rarity)
        {
            switch (_rarity)
            {
                case Rarity.Common:
                    return 1.0f;
                case Rarity.Rare:
                    return 2.0f;
                default:
                    return 1.0f;
            }
        } 
        #endregion
    }


}
