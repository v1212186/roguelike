﻿using UnityEngine;

namespace LevelsSystem
{
    public class LevelMap
    {

        #region Variables

        private readonly Cell[,] cells;
        public readonly Vector2Int Dimensions;

        #endregion

        #region Constructor

        public LevelMap(Vector2Int _dimensions)
        {
            Dimensions = _dimensions;
            cells = new Cell[Dimensions.x, Dimensions.y];
        }

        #endregion

        #region Methods

        public Cell GetCellAtPosition(Vector2Int _position)
        {
            if (!IsWithinBounds(_position))
            {
                return null;
            }
            return cells[_position.x, _position.y];
        }

        public void SetCellAtPosition(Cell _cell, Vector2Int _position)
        {
            cells[_position.x, _position.y] = _cell;
        }

        public bool IsWithinBounds(Vector2Int _position)
        {
            return _position.x >= 0 && _position.y >= 0 & _position.x < Dimensions.x &&
                   _position.y < Dimensions.y;
        }

        #endregion
    }
}
