﻿using EntitiesSystem;
using EntitiesSystem.EntityComponents.ItemComponent;
using UnityEngine;

namespace LevelsSystem
{
    public class LevelGenerator
    {
        #region Variables

        private readonly Game game;

        #endregion

        #region Constructor

        public LevelGenerator(Game _game)
        {
            game = _game;
        }

        #endregion

        #region Methods
        //TODO что делать с энитити которые будут спавнится? Здесь или при генерации уровня? 
        public Level GenerateLevel(Vector2Int _dimensions)
        {
            LevelMap levelMap = new LevelMap(_dimensions);

            for (int i = 1; i < _dimensions.x - 1; i++)
            {
                for (int j = 1; j < _dimensions.y - 1; j++)
                {
                    levelMap.SetCellAtPosition(new Cell(CellType.Floor, false, false), new Vector2Int(i, j));
                }
            }

            for (int i = 0; i < _dimensions.x; i++)
            {
                levelMap.SetCellAtPosition(new Cell(CellType.Wall, true, true), new Vector2Int(i, 0));
                levelMap.SetCellAtPosition(new Cell(CellType.Wall, true, true), new Vector2Int(i, _dimensions.y - 1));
            }

            for (int i = 0; i < _dimensions.y; i++)
            {
                levelMap.SetCellAtPosition(new Cell(CellType.Wall, true, true), new Vector2Int(0, i));
                levelMap.SetCellAtPosition(new Cell(CellType.Wall, true, true), new Vector2Int(_dimensions.x - 1, i));
            }

            levelMap.SetCellAtPosition(new Cell(CellType.Wall, true, true), new Vector2Int(4, 4));

            Level level = new Level(levelMap, game);

            GenerateEntitiesOnLevel(level);

            return level;
        }

        //TODO генерация энтитей
        public void GenerateEntitiesOnLevel(Level _level)
        {
            Entity entity = game.EntityGenerator.GenerateItemEntityForAbstractItem(game.ItemsGenerator
                .GenerateRandomEmpowerPotionItem());
            entity.PlaceEntityOnLevel(_level, new Vector2Int(1, 1));
            entity = game.EntityGenerator.GenerateItemEntityForAbstractItem(game.ItemsGenerator
                .GenerateRandomEmpowerPotionItem());
            entity.PlaceEntityOnLevel(_level, new Vector2Int(1, 2));
            entity = game.EntityGenerator.GenerateItemEntityForAbstractItem(game.ItemsGenerator
                .GenerateRandomEmpowerPotionItem());
            entity.PlaceEntityOnLevel(_level, new Vector2Int(2, 1));
            entity = game.EntityGenerator.GenerateItemEntityForAbstractItem(game.ItemsGenerator
                .GenerateRandomEmpowerPotionItem());
            entity.PlaceEntityOnLevel(_level, new Vector2Int(2, 2));
            entity = game.EntityGenerator.GenerateItemEntityForAbstractItem(game.ItemsGenerator
                .GenerateRandomEmpowerPotionItem());
            entity.PlaceEntityOnLevel(_level, new Vector2Int(2, 2));
            entity = game.EntityGenerator.GenerateDoorEntity(game.SpriteManager.GetRandomTileSet()
                .DoorOpenedClosedSprites);
            entity.PlaceEntityOnLevel(_level, new Vector2Int(4, 5));

            entity = game.EntityGenerator.GenerateTestTriggerEntity();
            entity.PlaceEntityOnLevel(_level, new Vector2Int(4, 6));
        }

        #endregion

    }
}
