﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using EntitiesSystem;
using EntitiesSystem.EntityComponents;
#if UNITY_EDITOR
using UnityEngine;
#endif

namespace LevelsSystem
{
    public enum CellType
    {
        Floor = 0, Wall = 1
    }

    //TODO разделить коллайдабл и неколлайдабл энтити на тайле
    public class Cell
    {
        #region Variables
        public readonly CellType cellType;
        private bool isCollidable;
        public bool IsCollidable { get { return isCollidable; } }
        private bool isBlockingView;
        public bool IsBlockingView { get { return isBlockingView; } }
        private readonly List<Entity> entities;
        public readonly ReadOnlyCollection<Entity> EntitiesReadOnly;
        #endregion

        #region Constructor

        public Cell(CellType _cellType, bool _isCollidable, bool _isBlockingView)
        {
            cellType = _cellType;
            isCollidable = _isCollidable;
            isBlockingView = _isBlockingView;
            entities = new List<Entity>();
            EntitiesReadOnly = entities.AsReadOnly();
        }

        #endregion

        #region Methods

        public bool RegisterEntity(Entity _entity)
        {
            if (entities.Contains(_entity))
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to register already registered entity: " + _entity + " at cell: " + this);
#endif
                return false;
            }
            entities.Add(_entity);
            return true;
        }

        public bool UnregisterEntity(Entity _entity)
        {
            return entities.Remove(_entity);
        }

        public void SetCollision(bool _value)
        {
            isCollidable = _value;
        }

        public void SetBlockingView(bool _value)
        {
            isBlockingView = _value;
        }

        public bool IsCollidableWithEntites()
        {
            if (isCollidable)
            {
                return true;
            }
            return GetCollidableEntities().Count > 0;
        }

        public List<Entity> GetCollidableEntities()
        {
            List<Entity> collidableEntities = new List<Entity>();
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].IsCollidable)
                {
                    collidableEntities.Add(entities[i]);
                }
            }
            return collidableEntities;
        }

        public bool IsBlockingViewWithEntites()
        {
            if (isCollidable)
            {
                return true;
            }
            return GetCollidableEntities().Count > 0;
        }

        public List<Entity> GetBlockingViewEntities()
        {
            List<Entity> blockingViewEntities = new List<Entity>();
            for (int i = 0; i < entities.Count; i++)
            {
                if (entities[i].IsBlockingView)
                {
                    blockingViewEntities.Add(entities[i]);
                }
            }
            return blockingViewEntities;
        }

        public T HasEntityWithEntityComponent<T>() where T : EntityComponent
        {
            for (int i = 0; i < entities.Count; i++)
            {
                EntityComponent entityComponent = entities[i].HasEntityComponent<T>();
                if (entityComponent != null)
                {
                    return entityComponent as T;
                }
            }
            return null;
        }

        public T[] HasEntitiesWithEntityComponent<T>() where T : EntityComponent
        {
            List<T> componentsResult = new List<T>();
            for (int i = 0; i < entities.Count; i++)
            {
                EntityComponent entityComponent = entities[i].HasEntityComponent<T>();
                if (entityComponent != null)
                {
                    componentsResult.Add(entityComponent as T);
                }
            }
            return componentsResult.ToArray();
        }

        #endregion

    }
}
