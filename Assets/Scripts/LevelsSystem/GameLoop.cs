﻿using System;
using System.Collections.Generic;
using EntitiesSystem;
using EntitiesSystem.EntityComponents.ActorComponent;
using UnityEngine;
using Utilities;

namespace LevelsSystem
{
    public class ActorChanged : EventArgs
    {
        #region Variables

        public readonly Entity previousActorEntity;
        public readonly Entity currentActorEntity;

        #endregion

        #region Constructor

        public ActorChanged(Entity _previousActorEntity, Entity _currentActorEntity)
        {
            previousActorEntity = _previousActorEntity;
            currentActorEntity = _currentActorEntity;
        }

        #endregion
    }

    public class GameLoop
    {
        #region Variables

        private readonly List<Actor> actors;
        private int currentActor;

        private Entity currentActivePlayerActor;

        #region Events

        public readonly GameEvent<object, ActorChanged> OnPlayerActorChanged = new GameEvent<object, ActorChanged>();

        #endregion

        #endregion

        #region Constructor

        public GameLoop()
        {
            actors = new List<Actor>();
            currentActor = 0;
        }

        #endregion

        #region Methods

        public void ProcessGameLoop()
        {
            GetCurrentActor().ProcessActor();
        }

        public void OnActorTurnEnded()
        {
            GetNextActorToAct();
        }

        private void SetCurrentActivePlayer(Entity _entity)
        {
            Entity previousActivePlayerActor = currentActivePlayerActor;
            currentActivePlayerActor = _entity;
            OnPlayerActorChanged.Raise(this, new ActorChanged(previousActivePlayerActor, currentActivePlayerActor));
        }

        public void RegisterActorForGameloop(Entity _entity)
        {
            if (!_entity.Actor || actors.Contains(_entity.Actor))
            {
                return;
            }
            actors.Add(_entity.Actor);
            _entity.Actor.OnActorTurnChanged.Add(OnActorTurnChanged);
        }

        private void OnActorTurnChanged(Entity _entity, ActorTurnChange _actorTurnChange)
        {
            if (_actorTurnChange.actorTurnChangeType == ActorTurnChange.ActorTurnChangeType.Ended)
            {
                OnActorTurnEnded();
            }
        }

        public void UnregisterActorFromGameloop(Entity _entity)
        {
            if (!_entity.Actor)
            {
                return;
            }
            if (actors.Contains(_entity.Actor))
            {
                int index = actors.IndexOf(_entity.Actor);
                if (index < currentActor)
                {
                    currentActor--;
                }
                actors.RemoveAt(index);
                _entity.Actor.OnActorTurnChanged.Remove(OnActorTurnChanged);
            }
        }

        private Actor GetCurrentActor()
        {
            if (actors.Count == 0)
            {
#if UNITY_EDITOR
                Debug.LogError("No entites registered for game loop");
#endif
                return null;
            }
            return actors[currentActor];
        }

        private void IncrementCurrentActor()
        {
            currentActor = (currentActor + 1) % actors.Count;
        }

        public void GetNextActorToAct()
        {
            while (Math.Abs(GetCurrentActor().Entity.CharacterAttributes.EnergyAttribute.CurrentValue - GetCurrentActor().Entity.CharacterAttributes.EnergyAttribute.FinalValue) > float.Epsilon)
            {
                GetCurrentActor().GainEnergy();
                IncrementCurrentActor();
            }
            Actor actor = GetCurrentActor();
            if (actor is PlayerActor)
            {
                SetCurrentActivePlayer(actor.Entity);
            }
            actor.StartTurn();
        }


        #endregion
    }
}
