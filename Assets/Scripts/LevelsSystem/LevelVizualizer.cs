﻿using SpritesSystem;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace LevelsSystem
{
    public class LevelVizualizer
    {

        #region Variables
        private Game game;
        private Tilemap tilemap;
        private TileSet currentTileSet;
        public TileSet CurrentTileSet { get { return currentTileSet; } }
        #endregion

        #region Constructor

        public LevelVizualizer(Tilemap _tilemap)
        {
            tilemap = _tilemap;
        }

        #endregion

        #region Methods

        public void SetTileSet(TileSet _tileSet)
        {
            currentTileSet = _tileSet;
        }

        public void VisualizeLevel(Level _level, TileSet _tileSet)
        {
            currentTileSet = _tileSet;

            for (int i = 0; i < _level.LevelMap.Dimensions.x; i++)
            {
                for (int j = 0; j < _level.LevelMap.Dimensions.y; j++)
                {
                    TileBase tile = null;
                    if (_level.LevelMap.GetCellAtPosition(new Vector2Int(i, j)).cellType == CellType.Floor)
                    {
                        tile = currentTileSet.FloorTile;
                    }
                    if (_level.LevelMap.GetCellAtPosition(new Vector2Int(i, j)).cellType == CellType.Wall)
                    {
                        tile = currentTileSet.WallTile;
                    }
                    tilemap.SetTile(new Vector3Int(i, j, 0), tile);
                }
            }
            //TODO для каждой двери(например) по левелу изменить тайлсет
        }

        #endregion
    }
}
