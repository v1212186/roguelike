﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EntitiesSystem;
using EntitiesSystem.EntityComponents;
using EntitiesSystem.EntityComponents.CharacterComponent;
using UnityEngine;
using Utilities;

namespace LevelsSystem
{
    public enum RegistationType
    {
        Register, Unregister
    }

    public class EntityRegistration : EventArgs
    {
        #region Variables

        public readonly RegistationType RegistationType;

        #endregion

        #region Constructor

        public EntityRegistration(RegistationType _registationType)
        {
            RegistationType = _registationType;
        }

        #endregion
    }

    public class Level
    {
        #region Variables
        public readonly Game Game;

        public readonly LevelMap LevelMap;

        private readonly List<Entity> entities;
        public readonly ReadOnlyCollection<Entity> EntitiesReadOnly;

        private Entity player;
        public Entity Player { get { return player; } }

        public readonly GameLoop GameLoop;

        private bool isPlaying;

        #region Events

        public readonly GameEvent<Entity, ValueChangeEventArgs<Vector2Int>> OnEntityPositionChangedGlobal = new GameEvent<Entity, ValueChangeEventArgs<Vector2Int>>();
        public readonly GameEvent<Entity, EntityRegistration> OnEntityRegistrationGlobal = new GameEvent<Entity, EntityRegistration>();

        #endregion
        #endregion

        #region Constructor

        public Level(LevelMap _levelMap, Game _game)
        {
            Game = _game;
            LevelMap = _levelMap;
            entities = new List<Entity>();
            EntitiesReadOnly = entities.AsReadOnly();
            GameLoop = new GameLoop();
            isPlaying = false;
        }

        #endregion

        #region Methods

        public void StartPlayingLevel()
        {
            isPlaying = true;
            GameLoop.GetNextActorToAct();
        }

        public void ProcessLevel()
        {
            if (!isPlaying)
            {
                return;
            }
            GameLoop.ProcessGameLoop();
        }

        #region Entites

        public void RegisterEntityOnLevel(Entity _entity)
        {
            if (entities.Contains(_entity))
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to register already registered entity: " + _entity);
#endif
                return;
            }
            entities.Add(_entity);
            GameLoop.RegisterActorForGameloop(_entity);
            _entity.OnEntityPositionChanged.Add(EntityPositionChanged);
            if (_entity.HasEntityComponent<Player>())
            {
                player = _entity;
            }
            OnEntityRegistrationGlobal.Raise(_entity, new EntityRegistration(RegistationType.Register));
        }

        public void UnregisterEntityFromLevel(Entity _entity)
        {
            if (!entities.Remove(_entity))
            {
#if UNITY_EDITOR
                Debug.LogError("Tried to unregister not registered entity: " + _entity);
#endif
                return;
            }
            GameLoop.UnregisterActorFromGameloop(_entity);
            _entity.OnEntityPositionChanged.Remove(EntityPositionChanged);
            if (_entity.HasEntityComponent<Player>())
            {
                //todo нужно ли плеера нулом делать7
                player = null;
            }
            OnEntityRegistrationGlobal.Raise(_entity, new EntityRegistration(RegistationType.Unregister));
        }

        private void EntityPositionChanged(Entity _entity, ValueChangeEventArgs<Vector2Int> _valueChangeEventArgs)
        {
            OnEntityPositionChangedGlobal.Raise(_entity, _valueChangeEventArgs);
        } 

        #endregion

        #region Cells

        public Entity GetEntityAtPositionWithComponent<T>(Vector2Int _position) where T : EntityComponent
        {
            Cell cell = GetCellAtPosition(_position);
            if (cell == null)
            {
                return null;
            }
            EntityComponent entityComponent = cell.HasEntityWithEntityComponent<T>();
            if (entityComponent != null)
            {
                return entityComponent.Entity;
            }
            return null;
        }


        public Entity[] GetEntitiesAtPositionWithComponent<T>(Vector2Int _position) where T : EntityComponent
        {
            Cell cell = GetCellAtPosition(_position);
            if (cell == null)
            {
                return null;
            }
            EntityComponent[] entityComponents = cell.HasEntitiesWithEntityComponent<T>();
            if (entityComponents != null && entityComponents.Length > 0)
            {
                Entity[] entitiesOnCell = new Entity[entityComponents.Length];
                for (int i = 0; i < entityComponents.Length; i++)
                {
                    entitiesOnCell[i] = entityComponents[i].Entity;
                }
                return entitiesOnCell;
            }
            return null;
        }

        //TODO делать ли проверки на множество кллойдабл энитити на одном тайле?
        public void RegisterEntityOnCell(Entity _entity, Vector2Int _position)
        {
            Cell cell = GetCellAtPosition(_position);
            if (cell == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Can't register entity at position: " + _position);
#endif
                return;
            }
            cell.RegisterEntity(_entity);
        }

        public void UnregisterEntityFromCell(Entity _entity, Vector2Int _position)
        {
            Cell cell = GetCellAtPosition(_position);
            if (cell == null)
            {
#if UNITY_EDITOR
                Debug.LogError("Can't unregister entity at position: " + _position);
#endif
                return;
            }
            cell.UnregisterEntity(_entity);
        }

        public Cell GetCellAtPosition(Vector2Int _position)
        {
            return LevelMap.GetCellAtPosition(_position);
        }

        public void SetCellAtPosition(Cell _cell, Vector2Int _position)
        {
            LevelMap.SetCellAtPosition(_cell, _position);
        }

        #endregion

        #endregion


    }
}
