﻿using System;
using System.Collections.Generic;

namespace Utilities
{
    public interface IEvent<TSender, TArgs>
    {
        void Add(Action<TSender, TArgs> _handler);
        void Remove(Action<TSender, TArgs> _handler);
    }

    public class GameEvent<TSender, TArgs> : IEvent<TSender, TArgs> where TArgs : EventArgs
    {
        public void Add(Action<TSender, TArgs> _handler)
        {
            if (mHandlers == null)
            {
                mHandlers = new List<Action<TSender, TArgs>>();
            }

            mHandlers.Add(_handler);
        }

        public void Remove(Action<TSender, TArgs> _handler)
        {
            if (mHandlers == null)
                throw new InvalidOperationException("Cannot remove a handler because it does not contain any.");

            mHandlers.Remove(_handler);
        }

        internal void Raise(TSender _sender, TArgs _args)
        {
            if (mHandlers != null)
            {
                mHandlers.ForEach((_action) => _action(_sender, _args));
            }
        }

        private List<Action<TSender, TArgs>> mHandlers;
    }

    public class ValueChangeEventArgs<TValue> : EventArgs
    {
        #region Variables

        public readonly TValue NewValue;
        public readonly TValue OldValue;

        #endregion

        #region Constructor

        public ValueChangeEventArgs(TValue _oldValueValue, TValue _newValueValue)
        {
            OldValue = _oldValueValue;
            NewValue = _newValueValue;
        }

        #endregion

    }
}