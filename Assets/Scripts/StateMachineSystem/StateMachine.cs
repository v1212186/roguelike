﻿namespace StateMachineSystem
{
    public class StateMachine
    {
        #region Variables

        private GameState gameState;

        #endregion

        #region Methods

        public void ProcessCurrentState()
        {
            if (gameState != null)
            {
                gameState.ProcessState();
            }
        }

        public void SetState(GameState _gameState)
        {
            if (gameState != null)
            {
                gameState.OnStateExit();
            }
            if (_gameState != null)
            {
                _gameState.OnStateEnter();
            }
            gameState = _gameState;

        }

        #endregion

    }
}
