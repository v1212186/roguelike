﻿
namespace StateMachineSystem.GameManagerStates
{
    public class ProcessingCurrentLevelState : GameState
    {


        #region Constructor
        public ProcessingCurrentLevelState(Game _game) : base(_game)
        {
        }
        #endregion

        #region Methods
        public override void ProcessState()
        {
            game.CurrentLevel.ProcessLevel();
        }

        public override void OnStateEnter()
        {
            game.UserInterfaceManager.InGamePanel.ShowInGamePanel();
            game.CurrentLevel.StartPlayingLevel();
        }

        public override void OnStateExit()
        {
            game.UserInterfaceManager.InGamePanel.CloseInGamePanel();
        }
        #endregion
    }
}
