﻿using EntitiesSystem;
using EntitiesSystem.EntityComponents.CharacterComponent;
using UnityEngine;

namespace StateMachineSystem.GameManagerStates
{
    public class PlacingEntitiesState : GameState
    {

        #region Constructor

        public PlacingEntitiesState(Game _game) : base(_game)
        {
        }
        #endregion

        #region Methods
        public override void ProcessState()
        {
            Entity player = game.EntityGenerator.GeneratePlayerActor();
            player.PlaceEntityOnLevel(game.CurrentLevel, new Vector2Int(5, 5));
            Entity character = game.EntityGenerator.GenerateCharacterActorWithPlayerActorController();
            character.PlaceEntityOnLevel(game.CurrentLevel, new Vector2Int(6, 6));
            //Character character2 = gameManager.CharacterGenerator.GenerateCharacterActor();
            //character2.PlaceEntity(gameManager.CurrentLevel, new Vector2Int(7, 7));
            game.StateMachine.SetState(game.ProcessingCurrentLevelState);
        }

        public override void OnStateEnter()
        {
        }

        public override void OnStateExit()
        {
        }
        #endregion
    }
}
