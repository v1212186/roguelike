﻿namespace StateMachineSystem.GameManagerStates
{
    public sealed class InitializeState : GameState
    {

        #region Constructors
        public InitializeState(Game _game) : base(_game)
        {
        }
        #endregion

        #region Methods
        public override void ProcessState()
        {
            game.StateMachine.SetState(game.MainMenuState);
        }

        public override void OnStateEnter()
        {
            game.UserInterfaceManager.SubscribeToEvents(game);
            game.CameraFollower.SubscribeToEvents(game);
        }

        public override void OnStateExit()
        {
        }
        #endregion
    }
}
