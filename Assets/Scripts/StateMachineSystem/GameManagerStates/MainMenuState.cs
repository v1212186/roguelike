﻿namespace StateMachineSystem.GameManagerStates
{
    public class MainMenuState : GameState
    {

        #region Constructors
        public MainMenuState(Game _game) : base(_game)
        {
        }
        #endregion
        
        #region Methods

        public override void ProcessState()
        {
        }

        public override void OnStateEnter()
        {
            game.UserInterfaceManager.MainMenuPanel.ShowMainMenuPanel();
        }

        public override void OnStateExit()
        {
            game.UserInterfaceManager.MainMenuPanel.CloseMainMenuPanel();
        } 
        #endregion
    }
}
