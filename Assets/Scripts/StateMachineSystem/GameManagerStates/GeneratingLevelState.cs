﻿using LevelsSystem;
using UnityEngine;

namespace StateMachineSystem.GameManagerStates
{
    public class GeneratingLevelState : GameState
    {


        #region Constructor
        public GeneratingLevelState(Game _game) : base(_game)
        {

        }
        #endregion

        #region Methods
        public override void ProcessState()
        {
            Level level = game.LevelGenerator.GenerateLevel(new Vector2Int(15, 15));
            game.CurrentLevel = level;
            game.LevelVizualizer.VisualizeLevel(level, game.SpriteManager.GetRandomTileSet());
            game.StateMachine.SetState(game.PlacingEntitiesState);
        }

        public override void OnStateEnter()
        {

        }

        public override void OnStateExit()
        {
        }
        #endregion
    }
}
