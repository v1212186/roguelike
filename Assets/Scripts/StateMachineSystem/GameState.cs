﻿namespace StateMachineSystem
{
    public abstract class GameState
    {
        #region Variables
        protected Game game;
        #endregion

        #region Constructor

        protected GameState(Game _game)
        {
            game = _game;
        }

        #endregion

        #region Methods
        public abstract void ProcessState();
        public abstract void OnStateEnter();
        public abstract void OnStateExit();
        #endregion
    }
}
