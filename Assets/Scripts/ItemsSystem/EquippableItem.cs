﻿using System.Collections.Generic;
using AttributesSystem;
using RaritySystem;
using UnityEngine;

namespace ItemsSystem
{
    public enum EquipmentSlot
    {
        Armor = 0, Boots = 1, Gloves = 2, Helmet = 3, Weapon = 4
    }

    public class EquippableItem : AbstractItem
    {
        //TODO всегда создается пустой лист модифаеров, а добавляются они через метод(для энчансментов)
        #region Variables
        public readonly EquipmentSlot equipmentSlot;
        private readonly List<AttributeModifier> attributeModifiers;
        public List<AttributeModifier> AttributeModifiers { get { return attributeModifiers; } }    //TODO сделать этот лист ридонли, а добавление через метод
        #endregion

        #region Constructors

        public EquippableItem(string _itemName, Sprite _itemSprite, Rarity _itemRarity, EquipmentSlot _equipmentSlot,
            List<AttributeModifier> _attributeModifiers) : base(_itemName, _itemSprite, _itemRarity)
        {
            equipmentSlot = _equipmentSlot;
            attributeModifiers = _attributeModifiers;
        }

        public EquippableItem(string _itemName, Sprite _itemSprite, Rarity _itemRarity, EquipmentSlot _equipmentSlot) :
            this(_itemName, _itemSprite, _itemRarity, _equipmentSlot, new List<AttributeModifier>())
        {

        }

        #endregion

        #region Methods

        public override string GetDescription()
        {
            string description = "";
            for (int i = 0; i < attributeModifiers.Count; i++)
            {
                description += attributeModifiers[i].AttributeType.ToString() + ": " + attributeModifiers[i].Value
                    .ToString("#0.00") + "\n";
            }
            return description;
        }

        #endregion


    }
}
