﻿using System.Collections.Generic;
using ActionsSystem;
using AttributesSystem;
using RaritySystem;
using SpritesSystem;
using StatusEffectsSystem;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ItemsSystem
{
    public class ItemsGenerator
    {

        #region Variables

        private readonly Game game;

        #endregion

        #region Constructor

        public ItemsGenerator(Game _game)
        {
            game = _game;
        }

        #endregion

        #region Methods
        public EquippableItem GenerateRandomEquippalbeItem()
        {
            //https://www.reddit.com/r/roguelikedev/comments/34oe75/item_generation/
            EquipmentSlot equipmentSlot = (EquipmentSlot)Random.Range(0, 5);
            Sprite sprite = game.SpriteManager.GetRandomEquippableItemSpriteBySlot(equipmentSlot);
            EquippableItem equippableItem = new EquippableItem("Random " + equipmentSlot.ToString(), sprite, Rarity.Common, equipmentSlot, new List<AttributeModifier>());
            AttributeModifier am = new AttributeModifier(Random.Range(1, 50), AttributeModifierType.Flat, AttributeType.Defence, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            am = new AttributeModifier(Random.Range(1, 50), AttributeModifierType.Flat, AttributeType.Attack, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            am = new AttributeModifier(Random.Range(1, 10), AttributeModifierType.Flat, AttributeType.Energy, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            am = new AttributeModifier(Random.Range(1, 50), AttributeModifierType.Flat, AttributeType.Health, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            am = new AttributeModifier(Random.Range(1, 15), AttributeModifierType.Flat, AttributeType.CriticalHit, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            am = new AttributeModifier(Random.Range(0, 1), AttributeModifierType.Flat, AttributeType.HitChance, equippableItem);
            equippableItem.AttributeModifiers.Add(am);
            return equippableItem;
        }

        public UsableItem GenerateRandomHealthPotionItem()
        {
            Sprite sprite = game.SpriteManager.GetRandomPotionSprite();
            UsableItem healthPotionItem = new UsableItem("Health potion", sprite, Rarity.Rare, new ReceiveHealStatusEffect(game.SpriteManager.GetRandomScrollSprite(),
                StatusEffectType.Beneficial, Random.Range(15, 41)));
            return healthPotionItem;
        }

        public UsableItem GenerateRandomEmpowerPotionItem()
        {
            Sprite sprite = game.SpriteManager.GetRandomPotionSprite();
            UsableItem healthPotionItem = new UsableItem("Empower potion", sprite, Rarity.Rare, new EmpowerStatusEffect(game.SpriteManager.GetRandomScrollSprite(), 5,
                StatusEffectType.Beneficial));
            return healthPotionItem;
        }
        #endregion
    }
}
