﻿using RaritySystem;
using UnityEngine;

namespace ItemsSystem
{
    //TODO может хранить лист действий(экшенов) или их ID в каком-то другом листе с итемом и возвращать этот лист в контекстном меню
    //TODO эти экшоны не должны быть все отдельными инстансами, а референсом на заранее созданные инстансы
    //TODO потом доступные экшоны обрабатывать в нужном методе, тогда класс итемов независим от всего остального
    public abstract class AbstractItem
    {
        #region Variables
        public readonly string ItemName;
        public readonly Sprite ItemSprite;
        public readonly Rarity ItemRarity;
        #endregion

        #region Constructors

        protected AbstractItem(string _itemName, Sprite _itemSprite, Rarity _itemRarity)
        {
            ItemName = _itemName;
            ItemSprite = _itemSprite;
            ItemRarity = _itemRarity;
        }

        public abstract string GetDescription();

        #endregion
    }
}
