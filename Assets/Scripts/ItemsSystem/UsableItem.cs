﻿using ActionsSystem;
using RaritySystem;
using StatusEffectsSystem;
using UnityEngine;

namespace ItemsSystem
{
    //TODO вместо метода, передавать экшон, например ввосстановить здоровье, или даже передавать статус эффект
    //TODO если буду передавать экшон, то все юзабл айтемы будут модульными
    //TODO для определенных типов итемов можно в конструкторе определить необходимые экшоны
    public class UsableItem : AbstractItem
    {
        #region Variables

        public readonly AbstractStatusEffect StatusEffect;

        #endregion

        #region Constructor

        public UsableItem(string _itemName, Sprite _itemSprite, Rarity _itemRarity, AbstractStatusEffect _statusEffect) : base(_itemName, _itemSprite, _itemRarity)
        {
            StatusEffect = _statusEffect;
        }

        #endregion
       
        #region Methods

        public override string GetDescription()
        {
            return StatusEffect.GetDescription();
        } 

        #endregion
    }
}
