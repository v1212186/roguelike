﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace AttributesSystem
{
    public enum AttributeType
    {
        Health, Energy, Attack, Defence, CriticalHit, HitChance
    }

    [Serializable]
    public abstract class AbstractAttribute
    {

        #region Variables
        [SerializeField]
        protected float baseValue;
        public virtual float BaseValue
        {
            get { return baseValue; }
            set
            {
                baseValue = value;
                needToRecalculate = true;
            }
        }

        public virtual float FinalValue
        {
            get
            {
                if (needToRecalculate)
                {
                    finalValue = CalculateFinalValue();
                    needToRecalculate = false;
                }
                return finalValue;
            }
        }

        protected bool needToRecalculate = true;
        [SerializeField]
        protected float finalValue;

        [SerializeField]
        protected AttributeType attributeType;
        public AttributeType AttributeType { get { return attributeType; } }

        protected readonly List<AttributeModifier> attributeModifiers;
        public readonly ReadOnlyCollection<AttributeModifier> attributeModifiersReadOnly;

        #endregion

        #region Constructors

        protected AbstractAttribute()
        {
            attributeModifiers = new List<AttributeModifier>();
            attributeModifiersReadOnly = attributeModifiers.AsReadOnly();
        }

        protected AbstractAttribute(float _baseValue) : this()
        {
            baseValue = _baseValue;
        }
        #endregion

        #region Methods
        public virtual void AddModifier(AttributeModifier _attributeModifier)
        {
            needToRecalculate = true;
            attributeModifiers.Add(_attributeModifier);
            attributeModifiers.Sort(CompareAttributeModifierOrder);
        }

        public virtual bool RemoveModifier(AttributeModifier _attributeModifier)
        {
            needToRecalculate = attributeModifiers.Remove(_attributeModifier);
            return needToRecalculate;
        }


        public virtual bool RemoveAllAttributeModifiersFromSource(object _source)
        {
            bool isRemoved = false;

            for (int i = attributeModifiers.Count - 1; i >= 0; i--)
            {
                if (attributeModifiers[i].Source == _source)
                {
                    needToRecalculate = true;
                    isRemoved = true;
                    attributeModifiers.RemoveAt(i);
                }
            }

            return isRemoved;
        }

        protected virtual float CalculateFinalValue()
        {
            finalValue = baseValue;
            float sumPercentAdd = 0;

            for (int i = 0; i < attributeModifiers.Count; i++)
            {
                switch (attributeModifiers[i].AttributeModifierType)
                {
                    case AttributeModifierType.Flat:
                        finalValue += attributeModifiers[i].Value;
                        break;
                    case AttributeModifierType.PercentAdd:
                        sumPercentAdd += attributeModifiers[i].Value;
                        if (i + 1 >= attributeModifiers.Count || attributeModifiers[i + 1].AttributeModifierType !=
                            AttributeModifierType.PercentAdd)
                        {
                            finalValue *= 1 + sumPercentAdd;
                            sumPercentAdd = 0;
                        }
                        break;
                    case AttributeModifierType.PercentMult:
                        finalValue *= 1 + attributeModifiers[i].Value;
                        break;
                    default: break;
                }
            }

            return Mathf.Round(finalValue);
        }

        protected int CompareAttributeModifierOrder(AttributeModifier _a, AttributeModifier _b)
        {
            if (_a.Order < _b.Order)
            {
                return -1;
            }
            if (_a.Order > _b.Order)
            {
                return 1;
            }
            return 0;   // if (_a.order == _b.order)
        }
        #endregion
    }
}
