﻿using System;

namespace AttributesSystem
{
    [Serializable]
    public class AttackAttribute : AbstractAttribute
    {
        #region Constructor

        public AttackAttribute() : base()
        {
            attributeType = AttributeType.Attack;
        }

        public AttackAttribute(float _baseValue) : base(_baseValue)
        {
            attributeType = AttributeType.Attack;
        }

        #endregion

    }
}
