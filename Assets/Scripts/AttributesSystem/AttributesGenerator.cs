﻿using EntitiesSystem.EntityComponents.CharacterAttributesComponent;

namespace AttributesSystem
{
    public class AttributesGenerator
    {

        #region Variables



        #endregion

        #region Methods

        public void GenerateAttributes(CharacterAttributes _characterAttributes, int _level)
        {
            HealthAttribute healthAttribute = new HealthAttribute(100);
            EnergyAttribute energyAttribute = new EnergyAttribute();
            AttackAttribute attackAttribute = new AttackAttribute(25);
            DefenceAttribute defenceAttribute = new DefenceAttribute(10);
            CriticalHitChanceAttribute criticalHitChanceAttribute = new CriticalHitChanceAttribute();
            HitChanceAttribute hitChanceAttribute = new HitChanceAttribute();
            _characterAttributes.Initialize(healthAttribute, energyAttribute, attackAttribute, defenceAttribute, criticalHitChanceAttribute, hitChanceAttribute);
        }

        public AbstractAttribute GenerateAttribute(AttributeType _attributeType, float _totalPower)
        {
            switch (_attributeType)
            {
                case AttributeType.Health:
                    return new HealthAttribute(100);
                case AttributeType.Energy:
                    return new EnergyAttribute();
                case AttributeType.Attack:
                    return new AttackAttribute(25);
                case AttributeType.Defence:
                    return new DefenceAttribute(10);
                case AttributeType.CriticalHit:
                    return new CriticalHitChanceAttribute();
                case AttributeType.HitChance:
                    return new HitChanceAttribute();
            }
            return null;
        }

        #endregion
    }
}
