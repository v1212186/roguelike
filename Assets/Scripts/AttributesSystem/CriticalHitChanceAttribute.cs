﻿using System;
using UnityEngine;

namespace AttributesSystem
{
    [Serializable]
    public class CriticalHitChanceAttribute : AbstractAttribute {

        #region Variables

        private const float ValueMaxCap = 100.0f;
        private const float ValueMinCap = 1.0f;
        private const float DefaultValue = 15.0f;

        #endregion

        #region Constructors

        public CriticalHitChanceAttribute() : base()
        {
            baseValue = DefaultValue;
            attributeType = AttributeType.CriticalHit;
        }

        #endregion

        #region Methods

        protected override float CalculateFinalValue()
        {
            return Mathf.Clamp(base.CalculateFinalValue(), ValueMinCap, ValueMaxCap);
        }

        #endregion
    }
}
