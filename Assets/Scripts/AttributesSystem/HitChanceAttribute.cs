﻿using System;
using UnityEngine;

namespace AttributesSystem
{
    [Serializable]
    public class HitChanceAttribute : AbstractAttribute
    {
        #region Variables

        private const float ValueMaxCap = 100.0f;
        private const float ValueMinCap = 10.0f;
        private const float DefaultValue = 90.0f;

        #endregion

        #region Constructors

        public HitChanceAttribute() : base()
        {
            baseValue = DefaultValue;
            attributeType = AttributeType.HitChance;
        }

        #endregion

        #region Methods

        protected override float CalculateFinalValue()
        {
            return Mathf.Clamp(base.CalculateFinalValue(), ValueMinCap, ValueMaxCap);
        }

        #endregion
    }
}
