﻿using System;

namespace AttributesSystem
{
    public enum AttributeModifierType
    {
        Flat = 0, PercentAdd = 1, PercentMult = 2
    }

    [Serializable]
    public class AttributeModifier
    {
        #region Varibles
        public readonly float Value;
        public readonly AttributeModifierType AttributeModifierType;
        public readonly AttributeType AttributeType;
        public readonly int Order;
        public readonly object Source;
        #endregion

        #region Constructors
        public AttributeModifier(float _value, AttributeModifierType _attributeModifierType, AttributeType _attributeType, int _order, object _source)
        {
            Value = _value;
            AttributeModifierType = _attributeModifierType;
            AttributeType = _attributeType;
            Order = _order;
            Source = _source;
        }

        public AttributeModifier(float _value, AttributeModifierType _attributeModifierType, AttributeType _attributeType, object _source) :
            this(_value, _attributeModifierType, _attributeType, (int)_attributeModifierType, _source)
        {

        }
        #endregion
    }
}
