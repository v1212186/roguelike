﻿using System;
using UnityEngine;

namespace AttributesSystem
{
    [Serializable]
    public class EnergyAttribute : AbstractAttribute
    {
        #region Variables
        [SerializeField]
        private float currentValue;
        public float CurrentValue { get { return currentValue; } }
        private const float DefaultValue = 100.0f;
        private const float ValueMinCap = 1.0f;
        #endregion


        #region Constructors
        public EnergyAttribute() : base()
        {
            baseValue = DefaultValue;
            currentValue = baseValue;
            attributeType = AttributeType.Energy;
        }
        #endregion

        #region Methods

        protected override float CalculateFinalValue()
        {
            finalValue = baseValue;
            float sumPercentAdd = 0;

            for (int i = 0; i < attributeModifiers.Count; i++)
            {
                switch (attributeModifiers[i].AttributeModifierType)
                {
                    case AttributeModifierType.Flat:
                        finalValue -= attributeModifiers[i].Value;
                        break;
                    case AttributeModifierType.PercentAdd:
                        sumPercentAdd += attributeModifiers[i].Value;
                        if (i + 1 >= attributeModifiers.Count || attributeModifiers[i + 1].AttributeModifierType !=
                            AttributeModifierType.PercentAdd)
                        {
                            finalValue /= 1 + sumPercentAdd;
                            sumPercentAdd = 0;
                        }
                        break;
                    case AttributeModifierType.PercentMult:
                        finalValue /= 1 + attributeModifiers[i].Value;
                        break;
                    default: break;
                }
            }

            return Mathf.Round(finalValue);
        }

        public float ChangeCurrentValue(float _value)
        {
            return currentValue = Mathf.Clamp(currentValue + _value, ValueMinCap, FinalValue);
        }

        #endregion
    }
}
