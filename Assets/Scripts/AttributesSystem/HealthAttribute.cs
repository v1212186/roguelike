﻿using System;
using UnityEngine;

namespace AttributesSystem
{
    [Serializable]
    public class HealthAttribute : AbstractAttribute
    {

        #region Variables
        [SerializeField]
        private float currentValue;
        public float CurrentValue { get { return currentValue; } }

        #endregion


        #region Constructors
        public HealthAttribute() : base()
        {
            attributeType = AttributeType.Health;
        }

        public HealthAttribute(float _baseValue) : base(_baseValue)
        {
            currentValue = _baseValue;
            attributeType = AttributeType.Health;
        }

        #endregion

        #region Methods

        public override void AddModifier(AttributeModifier _attributeModifier)
        {
            float currentPercentage = currentValue / FinalValue;
            base.AddModifier(_attributeModifier);
            currentValue = FinalValue * currentPercentage;
        }

        public override bool RemoveModifier(AttributeModifier _attributeModifier)
        {
            float currentPercentage = currentValue / FinalValue;
            needToRecalculate = base.RemoveModifier(_attributeModifier);
            currentValue = FinalValue * currentPercentage;
            return needToRecalculate;
        }

        public override bool RemoveAllAttributeModifiersFromSource(object _source)
        {
            float currentPercentage = currentValue / FinalValue;
            bool isRemoved = base.RemoveAllAttributeModifiersFromSource(_source);
            currentValue = FinalValue * currentPercentage;
            return isRemoved;
        }

        public float ChangeCurrentValue(float _value)
        {
            return currentValue = Mathf.Clamp(currentValue + _value, 0.0f, FinalValue);
        }

        #endregion

    }
}
